<?php 
    if (session_status() == PHP_SESSION_NONE) { session_start(); }

    require_once "../config/classes.php";

    $user = "";
    if ($_SESSION['id_utilisateur']) {
        $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
    }else{
        header("Location: ../");
    }

    /*Particulier*/
    $parti = $Particulier->getParticulierById($user['id_utilisateur']);
    /*Professionnelle*/
    $profes = $Professionnelle->getProfessionnelleById($user['id_utilisateur']);
    /*Professionnelle*/
    $ville = $Lieu->getLieuById($user['id_lieu']);
    /*Situation*/
    $sit = $Situation->getSituationById($user['id_situation']);
    /*Compétence utilisateur*/
    $competences = $Competence_Utilisateur->getCompetence_UtilisateurById($user['id_utilisateur']);

?>
<div class="row">

    <!-- Deuxième colonne -->
    <div class="col-md-4 order-md-2">
        <div class="card bg-light border mb-4">
            <div class="card-header align-center">
                <div class="row mb-4 mt-2">
                    <div class="col-md-4 col-sm-6">
                        <a href="#">
                            <img src="<?php echo '../assets/images/profil/'.$user['photo']; ?>" class="rounded-circle img-fluid border " alt="">
                        </a>
                    </div>
                    <div class="col-md-8 col-sm-6 py-2">
                        <a href="#">Mon compte</a>
                        <br>
                        <strong><?php echo $user['pseudo']; ?></strong>
                    </div>
                </div>
            </div>
            <div class="list-group">
                <a href="?profil" class="list-group-item list-group-item-action list-group-item-light active"><span class="mbri-user mr-3"></span> Mon profil</a>
                <a href="?identifiants" class="list-group-item list-group-item-action list-group-item-light"><span class="mbri-lock mr-3"></span> Mes identifiants</a>
                <a href="?messages" class="list-group-item list-group-item-action list-group-item-light"><span class="mbri-letter mr-3"></span> Mes messages</a>
                <a href="#" class="list-group-item list-group-item-action list-group-item-light" id="supprimer-compte"><span class="mbri-trash mr-3"></span> Supprimer mon compte</a>
            </div>
        </div>
    </div>
    <!-- Première colonne -->
    <div class="col-md-8 mb-5 order-md-1">
    
        <div class="card bg-light" >
            <div class="card-body border text-left">
                <h1 class="card-title">Mon profil : <small><?php echo $user['pseudo']; ?></small></h1>
                    
                <!-- Mes informations -->
                <div class="mb-2 mt-4">
                    <h2 class="card-subtitle mb-2 pb-2 text-muted border-bottom">
                        Modifier mon profil
                    </h2>
                </div>
                    <form action="?profil" method="POST" class="mt-3" enctype="multipart/form-data">
                        <!-- Image de profil -->
                        <div class="form-group row">
                            <label for="img_profil" class="col-sm-4 col-form-label">Image de profil</label>
                            <div class="col-sm-8">
                                <input type="file" name="img_profil" class="form-control form-control-sm" id="img_profil" >
                            </div>
                        </div>
                        <!-- Statut -->
                        <div class="form-group row" hidden>
                            <label for="statut" class="col-sm-4 col-form-label float-md-right">Statut</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm w-100 p-1" style="min-height: 10px" name="statut" id="statut" required > 
                                    <option value="" disabled selected>-- Choisissez votre statut --</option>
                                    <?php if (!empty($parti['id_utilisateur'])): ?>
                                        <option value="particulier" selected>Particulier</option>
                                        <!-- On parcourt la liste des communes -->
                                        <?php foreach ($lesTypeEtablissements as $typeEtablissement): ?>
                                            <option value="<?php echo $typeEtablissement['code_type_etablissement']; ?>"><?php echo $typeEtablissement['lib_etablissement']; ?></option>
                                        <?php endforeach ?>
                                    <?php else: ?>

                                        <option value="particulier">Particulier</option>
                                        <!-- On parcourt la liste des communes -->
                                        
                                        <?php foreach ($lesTypeEtablissements as $typeEtablissement): ?>
                                            <?php if ($typeEtablissement['id_etablissement'] == $profes['id_etablissement']): ?>
                                                <option value="<?php echo $typeEtablissement['code_type_etablissement']; ?>" selected><?php echo $typeEtablissement['lib_etablissement']; ?></option>
                                            <?php else: ?>
                                                <option value="<?php echo $typeEtablissement['code_type_etablissement']; ?>"><?php echo $typeEtablissement['lib_etablissement']; ?></option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                    
                                </select>
                            </div>
                        </div>
                        <!-- pseudo 
                        <div class="form-group row" hidden>
                            <label for="siret" class="col-sm-4 col-form-label">SIRET</label>
                            <div class="col-sm-8">
                                <input type="text" name="siret" value="<?php //echo $user['siret']; ?>" class="form-control form-control-sm" id="siret" placeholder="N°SIRET" required>
                            </div>
                        </div>
                        
                        <div class="form-group row" hidden>
                            <label for="raison_sociale" class="col-sm-4 col-form-label">Raison sociale</label>
                            <div class="col-sm-8">
                                <input type="text" name="raison_sociale" value="<?php //echo $user['raison_sociale']; ?>" class="form-control form-control-sm" id="raison_sociale" placeholder="Raison sociale" required>
                            </div>
                        </div> -->
                        <!-- pseudo -->
                        <div class="form-group row">
                            <label for="pseudo" class="col-sm-4 col-form-label">Pseudo *</label>
                            <div class="col-sm-8">
                                <input type="text" name="pseudo" value="<?php echo $user['pseudo']; ?>" class="form-control form-control-sm" id="pseudo" placeholder="Pseudo" required>
                            </div>
                        </div>
                        <!-- Civilité -->
                        <div class="form-group row">
                            <label for="civilite" class="col-sm-4 col-form-label">Civilité *</label>
                            <div class="col-sm-8">
                                <select class="form-control w-100 m-0 p-1 form-control-sm" id="civilite" style="min-height: 10px" name="civilite" required>
                                    <option value="" disabled selected>-- Civilité --</option>
                                    <?php if ($user['civilite'] == "m"): ?>
                                        <option value="m" selected>Monsieur</option>
                                        <option value="f">Madame</option>
                                    <?php elseif ($user['civilite'] == "f") : ?>
                                        <option value="m">Monsieur</option>
                                        <option value="f" selected>Madame</option>
                                    <?php else : ?>
                                        <option value="m">Monsieur</option>
                                        <option value="f">Madame</option>
                                    <?php endif ?>
                                    
                                </select>
                            </div>
                        </div>
                        <!-- Nom -->
                        <div class="form-group row">
                            <label for="nom" class="col-sm-4 col-form-label">Nom *</label>
                            <div class="col-sm-8">
                                <input type="text" name="nom" class="form-control form-control-sm" value="<?php echo $user['nom'];?>" id="nom" placeholder="Nom" required>
                            </div>
                        </div>
                        <!-- Prénom -->
                        <div class="form-group row">
                            <label for="prenom" class="col-sm-4 col-form-label">Prénom *</label>
                            <div class="col-sm-8">
                                <input type="text" name="prenom" class="form-control form-control-sm" value="<?php echo $user['prenom'];?>" id="prenom" placeholder="Prénom" required>
                            </div>
                        </div>
                        <!-- Date de naissance  -->
                        <div class="form-group row">
                            <label for="datenaissance" class="col-sm-4 col-form-label">Date de naissance *</label>
                            <div class="col-sm-8">
                                <input type="date" name="datenaissance" class="form-control form-control-sm" value="<?php echo $user['dnaissance']; ?>" id="datenaissance" placeholder="Titre de l'annonce" required>
                            </div>
                        </div>
                        <!-- Ville -->
                        <div class="form-group row">
                            <label for="ville" class="col-sm-4 col-form-label">Ville *</label>
                            <div class="col-sm-8">
                                <select class="listecommune form-control w-100 m-0" id="ville"  name="ville" required>
                                    <option></option>
                                    <!-- On parcourt la liste des lieux -->
                                   
                                    <?php foreach ($lesLieux as $lieu): ?>
                                        <?php if ($lieu['id_lieu']==$ville['id_lieu']): ?>
                                            <option value="<?php echo $lieu['id_lieu']; ?>" selected><?php echo $lieu['nom_lieu']." (".$lieu['cp'].")"; ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo $lieu['id_lieu']; ?>"><?php echo $lieu['nom_lieu']." (".$lieu['cp'].")"; ?></option>
                                        <?php endif ?>
                                        
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <!-- Situation -->
                        <div class="form-group row">
                            <label for="situation" class="col-sm-4 col-form-label">Situation</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm w-100 p-1" name="situation" style="min-height: 10px" id="situation" >
                                    <option value="" disabled selected>-- Choisissez votre situation --</option>
                                    <!-- On parcourt la liste des situtations -->
                                    <?php foreach ($lesSituations as $situation): ?>
                                        <?php if ($situation['id_situation'] == $sit['id_situation']): ?>
                                            <option value="<?php echo $situation['id_situation']; ?>" selected><?php echo $situation['lib_situation']; ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo $situation['id_situation']; ?>"><?php echo $situation['lib_situation']; ?></option>
                                        <?php endif ?>
                                        
                                    <?php endforeach ?>
                                </select>
                                <small>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="accept_situation" value="">
                                        <label class="form-check-label" for="accept_situation">J'accepte d'afficher ma situation</label>
                                    </div>
                                </small>
                            </div>
                        </div>
                        <!-- Téléphone -->
                        <div class="form-group row">
                            <label for="tel" class="col-sm-4 col-form-label">Téléphone</label>
                            <div class="col-sm-8">
                                <input type="text" name="tel" class="form-control form-control-sm" id="tel" value="<?php echo $user['tel'];?>" placeholder="ex:06 39 00 00 00" >
                                <small>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="accept_tel" value="">
                                        <label class="form-check-label" for="accept_tel">J'accepte d'être contacté par téléphone</label>
                                    </div>
                                </small>
                            </div>
                        </div>
                        <!-- Compétences -->
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Compétences / Domaines d'activité</label>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <div class="" >
                                        <!-- on parcourt la liste des compétences -->
                                        <?php foreach ($lesCompetences as $competence): ?>
                                            <div class="row">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" name="competences[]" id="comp<?php echo $competence['id_competence']; ?>" value="<?php echo $competence['id_competence']; ?>"

                                                        <?php foreach ($competences as $comp){ 
                                                            if ($competence['id_competence'] == $comp['id_competence']){
                                                                echo "checked";
                                                            }else{
                                                                echo "";
                                                            }
                                                        } ?>
                                                    >
                                                    <label class="form-check-label" for="comp<?php echo $competence['id_competence']; ?>"><?php echo $competence['lib_competence']; ?></label>
                                                </div>
                                            </div>
                                        <?php endforeach ?>
                                        <!-- -------------------------------- -->
                                    </div>     
                                </div>
                            </div>
                        </div>
                        <!-- Expérience et références  -->
                        <div class="form-group row">
                            <label for="experience" class="col-sm-4 col-form-label">Expérience et références</label>
                            <div class="col-sm-8">
                                <textarea  name="experience" class="form-control form-control-sm" id="experience" placeholder="..." ><?php echo $user['description']; ?></textarea> 
                            </div>
                        </div>
                        <!-- Button Enregistrer -->
                        <div class="row align-center">
                            <div class="col-md-6 offset-md-3 input-group-btn">
                                <button type="submit" name="modifierUser" class="btn btn-primary p-1"><i class="fa fa-save mr-3"></i> Enregister</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
