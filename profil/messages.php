<div class="row">

    <!-- Deuxième colonne -->
    <div class="col-md-4 order-md-2">
        <div class="card bg-light border mb-4">
            <div class="card-header align-center">
                <div class="row mb-4 mt-2">
                    <div class="col-md-4 col-sm-6">
                        <a href="#">
                            <img src="<?php echo '../assets/images/profil/'.$user['photo']; ?>" class="rounded-circle img-fluid border " alt="">
                        </a>
                    </div>
                    <div class="col-md-8 col-sm-6 py-2">
                        <a href="#">Mon compte</a>
                        <br>
                        <strong><?php echo $user['pseudo']; ?></strong>
                    </div>
                </div>
            </div>
            <div class="list-group">
                <a href="?profil" class="list-group-item list-group-item-action list-group-item-light"><span class="mbri-user mr-3"></span> Mon profil</a>
                <a href="?identifiants" class="list-group-item list-group-item-action list-group-item-light"><span class="mbri-lock mr-3"></span> Mes identifiants</a>
                <a href="?messages" class="list-group-item list-group-item-action list-group-item-light active"><span class="mbri-letter mr-3"></span> Mes messages</a>
                <a href="#" class="list-group-item list-group-item-action list-group-item-light" id="supprimer-compte"><span class="mbri-trash mr-3"></span> Supprimer mon compte</a>
            </div>
        </div>

    </div>
    <!-- Première colonne  -->
    <div class="col-md-8 mb-5 order-md-1">
    
        <div class="card bg-light" >
            <div class="card-body border text-left">
                <h1 class="card-title">Ma Messagerie</h1>

                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><span class="mbri-database mr-2"></span> Tous les messages (0)</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"><span class="mbri-letter mr-2"></span> Messages non lu (0)</a>
                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"><span class="mbri-paper-plane mr-2"></span> Messages envoyés (0)</a>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                    </div>
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">

                    </div>
                </div>
                                                                  
            </div>
        </div>
    </div>
</div> 