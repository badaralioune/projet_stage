<?php 
    if (session_status() == PHP_SESSION_NONE) { session_start(); }

    require_once "../config/classes.php";

    $user = "";
    if ($_SESSION['id_utilisateur']) {
        $userUpdate = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
        $messageModicationMail = "";
        if(isset($_POST['ModifierMail'])){
            if($_POST['new-mail'] != $userUpdate['mail']){
                $messageModicationMail = '<div class="alert alert-success alert-dismissible fade show p-2" role="alert">
                                Votre adresse e-mail mis à jour !  
                                <button type="button" class="close p-2" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>';
                $Utilisateur->setUpdateMail($userUpdate['id_utilisateur'],$_POST['new-mail']);
            }else{
                $messageModicationMail = '<div class="alert alert-warning alert-dismissible fade show p-2" role="alert">
                                Votre n\'a pas été changer !
                                <button type="button" class="close p-2" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>';
            }
        }
        $messageModicationMDP = "";
        if (isset($_POST['ModifierMDP'])) {
            if ($_POST['pwd1'] != "") {
                $messageModicationMDP = '<div class="alert alert-success alert-dismissible fade show p-2" role="alert">
                                Votre mot de passe est mis à jour !  
                                <button type="button" class="close p-2" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>';
                $Utilisateur->setUpdateMDP($userUpdate['id_utilisateur'],md5($_POST['pwd1']));
            }
        }
        $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
    }else{
        header("Location: ../");
    }

    /*Particulier*/
    $parti = $Particulier->getParticulierById($user['id_utilisateur']);
    /*Professionnelle*/
    $profes = $Professionnelle->getProfessionnelleById($user['id_utilisateur']);
    /*Professionnelle*/
    $ville = $Lieu->getLieuById($user['id_lieu']);
    /*Situation*/
    $sit = $Situation->getSituationById($user['id_situation']);
    /*Compétence utilisateur*/
    $competences = $Competence_Utilisateur->getCompetence_UtilisateurById($user['id_utilisateur']);


?>
<div class="row">

    <!-- Deuxième colonne -->
    <div class="col-md-4 order-md-2">
        <div class="card bg-light border mb-4">
            <div class="card-header align-center">
                <div class="row mb-4 mt-2">
                    <div class="col-md-4 col-sm-6">
                        <a href="#">
                            <img src="<?php echo '../assets/images/profil/'.$user['photo']; ?>" class="rounded-circle img-fluid border " alt="">
                        </a>
                    </div>
                    <div class="col-md-8 col-sm-6 py-2">
                        <a href="#">Mon compte</a>
                        <br>
                        <strong><?php echo $user['pseudo']; ?></strong>
                    </div>
                </div>
            </div>
            <div class="list-group">
                <a href="?profil"  class="list-group-item list-group-item-action list-group-item-light"><span class="mbri-user mr-3"></span> Mon profil</a>
                <a href="?identifiants" class="list-group-item list-group-item-action list-group-item-light active"><span class="mbri-lock mr-3"></span> Mes identifiants</a>
                <a href="?messages"  class="list-group-item list-group-item-action list-group-item-light"><span class="mbri-letter mr-3"></span> Mes messages</a>
                <a href="#" class="list-group-item list-group-item-action list-group-item-light" id="supprimer-compte"><span class="mbri-trash mr-3"></span> Supprimer mon compte</a>
            </div>
        </div>

    </div>
    <!-- première colonne -->
    <div class="col-md-8 mb-5 order-md-1">
    
        <div class="card bg-light" >
            <div class="card-body border text-left">
                <h1 class="card-title">Mes identifiants</h1>

                
                <!-- Mes informations -->
                <div class="mb-2  mt-4">
                    <h2 class="card-subtitle my-2 pb-2 text-muted border-bottom">
                        Mes coordonnées
                    </h2>
                    <div class="card-body mt-2">
                        <?php echo $messageModicationMail; ?>
                        <form action="?identifiants" method="POST">
                            <div class="row mb-4 mt-2">
                                <div class="col-md-5 col-sm-5">
                                    <label for="mail">Votre adresse mail :</label>
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <input type="email" class="form-control" id="mail" value="<?php echo $user['mail']; ?>" disabled>
                                </div>
                            </div>

                            <div class="row mb-4 mt-2">
                                <div class="col-md-5 col-sm-5">
                                    <label for="lepwd">Mot de passe * :</label>
                                </div>
                                <div class="input-group col-md-7 col-sm-7">
                                    <input type="password" name="lepwd" class="form-control  p-2" id="lepwd" required aria-describedby="button-addon">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary py-0 px-2 m-0 rounded-right" title="Afficher le mot passe" type="button" id="button-addon"><i id="iconCacherMDP" class="fa fa-eye"></i></button>
                                    </div>
                                    <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_lemdp" class="text-white"></small></div>
                                    <input type="hidden" value="" id="lancienMDP">
                                </div>
                            </div>

                            <div class="row mb-4 mt-2">
                                <div class="col-md-5 col-sm-5">
                                    <label for="new-mail">Nouvelle adresse mail :</label>
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <input type="email" name="new-mail" class="form-control" id="new-mail" required>
                                </div>
                            </div>

                            


                            <div class="align-center">
                                <button type="submit" name="ModifierMail" class="btn btn-primary p-1">Enregistrer</button>
                            </div>
                        </form>
                    </div>

                    <!-- Modifier mon mot de passe -->
                    <h2 class="card-subtitle my-2 pb-2 text-muted border-bottom">
                        Modifier mon mot de passe
                    </h2>
                    <div class="card-body mt-2">
                        <?php echo $messageModicationMDP; ?>
                        <form action="?identifiants" method="POST" id="formMDP">


                            <div class="row mb-4 mt-2">
                                <div class="col-md-5 col-sm-5">
                                    <label for="pwd">Ancien mot de passe * :</label>
                                </div>
                                <div class="input-group col-md-7 col-sm-7">
                                    <input type="password" name="pwd" class="form-control  p-2" id="pwd" required aria-describedby="button-addon1">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary py-0 px-2 m-0 rounded-right" title="Afficher le mot passe" type="button" id="button-addon1"><i id="iconCacherMDP1" class="fa fa-eye"></i></button>
                                    </div>
                                    <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_mdp" class="text-white"></small></div>
                                    <input type="hidden" value="" id="ancienMDP">
                                </div>
                            </div>


                            <div class="row mb-4 mt-2">
                                <div class="col-md-5 col-sm-5">
                                    <label for="pwd1">Nouveau mot de passe * :</label>
                                </div>
                                <div class="input-group col-md-7 col-sm-7">
                                    <input type="password" name="pwd1" class="form-control p-2" id="pwd1" required aria-describedby="button-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary py-0 px-2 m-0 rounded-right" title="Afficher le mot passe" type="button" id="button-addon2"><i id="iconCacherMDP2" class="fa fa-eye"></i></button>
                                    </div>
                                    <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_mdp1" class="text-white"></small></div>
                                </div>
                            </div>


                            <div class="row mb-4 mt-2">
                                <div class="col-md-5 col-sm-5">
                                    <label for="confirm-pwd">Confirmer votre nouveau mot de passe * :</label>
                                </div>
                                <div class="input-group col-md-7 col-sm-7">
                                    <input type="password" name="confirm-pwd" class="form-control p-2" id="confirm-pwd" required aria-describedby="button-addon3">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary py-0 px-2 m-0 rounded-right" title="Afficher le mot passe" type="button" id="button-addon3"><i id="iconCacherMDP3" class="fa fa-eye"></i></button>
                                    </div>
                                    <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_mdp2" class="text-white"></small></div>
                                    <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_mdpdifferents" class="text-white verif7"></small></div>
                                </div>
                            </div>

                            <div class="align-center">
                                <button type="submit" name="ModifierMDP" class="btn btn-primary p-1">Confirmer</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div> 