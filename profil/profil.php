<?php 
    if (session_status() == PHP_SESSION_NONE) { session_start(); }

    require_once "../config/fonctions.php";

    $user = "";
    if ($_SESSION['id_utilisateur']) {

        $messageModication = "";
        // insert des données dans la base de données
        if (isset($_POST['modifierUser'])) {
            $id_utilisateur = $_SESSION['id_utilisateur'];
            $statut = $_POST['statut'];
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $civilite = $_POST['civilite'];
            $dnaissance = $_POST['datenaissance'];
            $adresse = "";
            if (isset($_POST['adresse'])) {
                $adresse = $_POST['adresse'];
            }
            $tel = $_POST['tel'];
            $pseudo = $_POST['pseudo'];
            $description = "";
            if (isset($_POST['experience'])) {
                $description = $_POST['experience'];
            }
            $photo = "";
            //Les données d'utilisateur avant modification
            $donneeBefore = $Utilisateur->getUtilisateurById($id_utilisateur);
            if (isset($_FILES['img_profil']['tmp_name']) && $_FILES['img_profil']['name'] != "") {
                //var_dump($_FILES['img_profil']['tmp_name']);
                
                
                $newFile = $donneeBefore['prenom'].date("dmYHis").'.'.pathinfo($_FILES['img_profil']['name'])['extension'];

                $retour = copy($_FILES['img_profil']['tmp_name'], "../assets/images/profil/".$newFile);
                if($retour) {
                    if ($donneeBefore['photo'] == "img_profil_defaut.png") {  
                        $photo = $newFile;              
                    }else{
                        if (file_exists('../assets/images/profil/'.$donneeBefore['photo'])) {
                            unlink('../assets/images/profil/'.$donneeBefore['photo']);
                        }
                        
                        $photo = $newFile; 
                    }
                }
            }else{
                $photo = $donneeBefore['photo']; 
            }

            $id_lieu = null;
            if (isset($_POST['ville']) && $_POST['ville'] != "") {
                $id_lieu = $_POST['ville'];
            }

            $id_situation = null;
            if (isset($_POST['situation'])) {
                $id_situation = $_POST['situation'];
            }
            
            //var_dump($id_utilisateur,$nom,$prenom,$civilite,$dnaissance,$adresse,$tel,$pseudo,$description,$photo,$id_lieu,$id_situation); die();
            // Update Utilisateur 
            $Utilisateur->UpdateUtilisateur($id_utilisateur,$nom,$prenom,$civilite,$dnaissance,$adresse,$tel,$pseudo,$description,$photo,$id_lieu,$id_situation);
            // Update Competences
            if (isset($_POST['competences'])) {
                updateCompetences($id_utilisateur, $_POST['competences']);
            }
            

            $userUpdate = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);

            //Si ce n'est pas particulier
            if ($statut != "particulier") {
                // On supprime les données dans la table particulier
                // On insère les données dans la table professionnelle
                //$Professionnelle->setProfessionnelle($mail,$tel,$pseudo,$mdp,$siret,$raison_sociale,$statut);
            }else{
                // On supprime les données dans la table professionnelle
                $Professionnelle->deleteProfessionnelle($id_utilisateur);
                // On insére statut perticulier
                $Particulier->setParticulierUpdate($id_utilisateur, $nom, $prenom, $civilite, $dnaissance, $adresse, $userUpdate['mail'], $tel, $pseudo, $userUpdate['mdp'], $description, $photo, $id_situation);
            } 

            $messageModication = '<div class="alert alert-success alert-dismissible fade show p-2" role="alert">
                            Vos informations ont été mises à jour !  
                            <button type="button" class="close p-2" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>';
        }

        if (isset($_POST['insert_img'])) {
            $id_utilisateur = $_SESSION['id_utilisateur'];
            $photo = "";
            //Les données d'utilisateur avant modification
            $donneeBefore = $Utilisateur->getUtilisateurById($id_utilisateur);
            if (isset($_FILES['img_profil']['tmp_name']) && $_FILES['img_profil']['name'] != "") {
                //var_dump($_FILES['img_profil']['tmp_name']);
                
                
                $newFile = $donneeBefore['prenom'].date("dmYHis").'.'.pathinfo($_FILES['img_profil']['name'])['extension'];

                $retour = copy($_FILES['img_profil']['tmp_name'], "../assets/images/profil/".$newFile);
                if($retour) {
                    if ($donneeBefore['photo'] == "img_profil_defaut.png") {  
                        $photo = $newFile;              
                    }else{
                        if (file_exists('../assets/images/profil/'.$donneeBefore['photo'])) {
                            unlink('../assets/images/profil/'.$donneeBefore['photo']);
                        }
                        
                        $photo = $newFile; 
                    }
                }
            }else{
                $photo = $donneeBefore['photo']; 
            }
            $Utilisateur->UpdateEtape4Utilisateur($id_utilisateur,$photo);
            var_dump($id_utilisateur,$photo); die();
        }

        $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
    }else{
        header("Location: ../");
    }

    /*Particulier*/
    $parti = $Particulier->getParticulierById($user['id_utilisateur']);
    /*Professionnelle*/
    $profes = $Professionnelle->getProfessionnelleById($user['id_utilisateur']);
    /*Compétence utilisateur*/
    $lesComps = $Competence_Utilisateur->getCompetence_UtilisateurById($user['id_utilisateur']);

?>
<div class="row">

    <!-- Deuxième colonne -->
    <div class="col-md-4 order-md-2">
        <div class="card bg-light border mb-4">
            <div class="card-header align-center">
                <div class="row mb-4 mt-2">
                    <div class="col-md-4 col-sm-6">
                        <a href="#">
                            <img src="<?php echo '../assets/images/profil/'.$user['photo']; ?>" class="rounded-circle img-fluid border " alt="">
                        </a>
                    </div>
                    <div class="col-md-8 col-sm-6 py-2">
                        <a href="#">Mon compte</a>
                        <br>
                        <strong><?php echo $user['pseudo']; ?></strong>
                    </div>
                </div>
            </div>
            <div class="list-group">
                <a href="?profil" class="list-group-item list-group-item-action list-group-item-light active"><span class="mbri-user mr-3"></span> Mon profil</a>
                <a href="?identifiants" class="list-group-item list-group-item-action list-group-item-light"><span class="mbri-lock mr-3"></span> Mes identifiants</a>
                <a href="?messages" class="list-group-item list-group-item-action list-group-item-light"><span class="mbri-letter mr-3"></span> Mes messages</a>
                <a href="#" class="list-group-item list-group-item-action list-group-item-light" id="supprimer-compte"><span class="mbri-trash mr-3"></span> Supprimer mon compte</a>
            </div>
        </div>
    </div>
    <!-- Première colonne -->
    <div class="col-md-8 mb-5 order-md-1 ">
    
        <div class="card bg-light" >
            <div class="card-body border text-left">
                <h1 class="card-title">Mon profil : <small><?php echo $user['pseudo']; ?></small></h1>
                    
                <!-- Mes informations -->
                <div class="mb-2 mt-4">
                    <h2 class="card-subtitle mb-2 pb-2 text-muted border-bottom">
                        <span class="float-right">
                            <a href="?edit" class="display-7 border p-2">
                                <span class="mbri-user"></span> Modifier mon profil
                            </a>
                        </span>Mes informations
                    </h2>
                    <div class="card-body mt-2">

                        <?php echo $messageModication; ?>

                        <div class="row border-dashed-bottom mb-4 mt-2">
                            <div class="col-md-4 col-sm-4">
                                Image de profil
                            </div>
                            <div class="col-md-8 col-sm-8 p-2">
                                <img src="<?php echo '../assets/images/profil/'.$user['photo']; ?>" class="rounded-circle img-fluid border w-50" alt="">
                            </div>
                        </div>

                        <div class="row border-dashed-bottom mb-4 mt-2">
                            <div class="col-md-4 col-sm-4">
                                Statut
                            </div>
                            <div class="col-md-8 col-sm-8">

                                <?php if (!empty($parti['id_utilisateur'])): ?>
                                    Particulier
                                <?php else: ?>
                                    <?php foreach ($lesTypeEtablissements as $typeEtablissement): ?>
                                        <?php if ($typeEtablissement['id_etablissement'] == $profes['id_etablissement']): ?>
                                            <?php echo $typeEtablissement['lib_etablissement']; ?>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                <?php endif ?>

                            </div>
                        </div>

                        <?php if (!empty($parti['id_utilisateur'])): ?>

                        <?php else: ?>

                            <div class="row border-dashed-bottom mb-4 mt-2">
                                <div class="col-md-4 col-sm-4">
                                    SIRET
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <?php echo $profes['num_siret']; ?>
                                </div>
                            </div>

                            <div class="row border-dashed-bottom mb-4 mt-2">
                                <div class="col-md-4 col-sm-4">
                                    Raison sociale
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <?php echo $profes['raison_sociale']; ?>
                                </div>
                            </div>

                        <?php endif ?>

                        <div class="row border-dashed-bottom mb-4 mt-2">
                            <div class="col-md-4 col-sm-4">
                                Pseudo
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <?php echo $user['pseudo']; ?>
                            </div>
                        </div>

                        <div class="row border-dashed-bottom mb-4 mt-2">
                            <div class="col-md-4 col-sm-4">
                                Je suis
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <?php 
                                    if ($user['civilite'] == "m") {
                                        echo "un homme";
                                    }elseif($user['civilite'] == "f"){
                                        echo "une femme";
                                    }else{
                                        echo "";
                                    }
                                    
                                ?>
                            </div>
                        </div>

                        <div class="row border-dashed-bottom mb-4 mt-2">
                            <div class="col-md-4 col-sm-4">
                                Prénom
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <?php echo $user['prenom']; ?>
                            </div>
                        </div>

                        <div class="row border-dashed-bottom mb-4 mt-2">
                            <div class="col-md-4 col-sm-4">
                                Nom
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <?php echo $user['nom']; ?>
                            </div>
                        </div>

                        <div class="row border-dashed-bottom mb-4 mt-2">
                            <div class="col-md-4 col-sm-4">
                                Date de naissance
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <?php echo $user['dNais']; ?>
                            </div>
                        </div>

                        <div class="row border-dashed-bottom mb-4 mt-2">
                            <div class="col-md-4 col-sm-4">
                                Téléphone
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <?php echo $user['tel']; ?>
                            </div>
                        </div>

                        <div class="row border-dashed-bottom mb-4 mt-2">
                            <div class="col-md-4 col-sm-4">
                                Ville
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <?php 
                                    $lieu = $Lieu->getLieuById($user['id_lieu']);
                                    if (!empty($lieu)) {
                                        echo $lieu['nom_lieu']." (".$lieu['cp'].")";
                                    }
                                    
                                ?>
                            </div>
                        </div>

                        <div class="row border-dashed-bottom mb-4 mt-2">
                            <div class="col-md-4 col-sm-4">
                                Situation
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <?php 
                                    $situation = $Situation->getSituationById($user['id_situation']);
                                    if (!empty($situation)) {
                                        echo $situation['lib_situation'];
                                    }
                                    
                                ?>
                            </div>
                        </div>

                        <div class="row border-dashed-bottom mb-4 mt-2">
                            <div class="col-md-4 col-sm-4">
                                Compétences :
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <div class="card-columns">
                                    <?php foreach ($lesComps as $comp): ?>
                                        <div class="card bg-warning m-1">
                                            <div class="card-body text-center p-0">
                                                <p class="card-text p-0">
                                                    <small><?php echo $comp['lib_competence']; ?></small>
                                                </p>
                                            </div>
                                        </div>
                                    <?php endforeach ?>
                                </div>

                            </div>
                        </div>

                        <div class="row border-dashed-bottom mb-4 mt-2">
                            <div class="col-md-4 col-sm-4">
                                Expérience et références :
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <?php echo $user['description']; ?> 
                            </div>
                        </div>


                    </div>
                </div>

                <!-- Button -->
                <div class="mb-4 align-center">
                    <a href="?edit" class="display-7 border p-2">
                        <span class="mbri-user"></span> Modifier mon profil
                    </a>
                </div> 

            </div>
        </div>
    </div>
</div>