<?php 
    require_once "../config/classes.php";
    $connexion = false;
    $user = [];
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        if (isset($_SESSION['id_utilisateur'])) {
            $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
            $connexion = true;
        }else{
            header("Location: ../");
        }
    }

?>
<!DOCTYPE html>
<html  >
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="../assets/images/logo2.png" type="image/x-icon">
        <meta name="description" content="">
        <title>Mon profil</title>
        <link rel="stylesheet" href="../assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="../assets/socicon/css/styles.css">
        <link rel="stylesheet" href="../assets/dropdown/css/style.css">
        <link rel="stylesheet" href="../assets/as-pie-progress/css/progress.min.css">
        <link rel="stylesheet" href="../assets/tether/tether.min.css">
        <link rel="stylesheet" href="../assets/theme/css/style.css">
        <link rel="preload" as="style" href="../assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- select 2 -->
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
        <style>   
            .border-dashed-bottom{
                border-bottom: 1px dashed black;
            }      
        </style>
    </head>
    <body>
        <section class="menu cid-rVmtRueE1z" once="menu" id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="../"><img src="../assets/images/logo2.png" alt="Nom du site" style="height: 3.8rem;"></a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-4" href="../">NOM DU SITE</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../#features1-8"><span class="mbri-bulleted-list mbr-iconfont mbr-iconfont-btn"></span>Toutes les demandes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../deposer_annonce/"><span class="mbri-edit mbr-iconfont mbr-iconfont-btn"></span>Déposer une annonce</a>
                        </li>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-black display-4" href="../profil/">
                                    <span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span> <?php echo $user['pseudo']; ?>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../authentification/sign_up.php"><span class="mbri-plus mbr-iconfont mbr-iconfont-btn"></span>Inscription</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../authentification/login.php"><span class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>Connexion</a>
                            </li>   
                        <?php endif ?>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../config/deconnexion.php"><span class="mbri-logout mbr-iconfont mbr-iconfont-btn"></span>Déconnexion</a>
                            </li>
                        <?php endif ?>
                    </ul>
                </div>
            </nav>
        </section>
        <section class="engine">
            <a href="#">website maker</a>
        </section>

        <section class="features1 cid-rVmE73Fmjs" id="depot_annonce">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 mt-5">
                        <div class="form-container">
                            <div class="media-container-column" data-form-type="formoid">
                                <!-- Mon annonce -->
                                <div class="col-md-12">
                                    <?php

                                        $page= getenv("QUERY_STRING");
                                        if($page=="") 
                                        $page="profil";
                                        require_once $page.".php";
                                    ?>
                                </div>
                            </div>
                        </div>
                                
                    </div>
                </div>
            </div>
        </section>

        <!-- Section footer -->
        <?php 
            require_once "../footer/footer_sous-racine.php";
        ?>
        <!-- Fin section footer -->
        <script src="../assets/web/assets/jquery/jquery.min.js"></script>
        <script src="../assets/popper/popper.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/smoothscroll/smooth-scroll.js"></script>
        <script src="../assets/dropdown/js/nav-dropdown.js"></script>
        <script src="../assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="../assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="../assets/parallax/jarallax.min.js"></script>
        <script src="../assets/as-pie-progress/jquery-as-pie-progress.min.js"></script>
        <script src="../assets/mbr-flip-card/mbr-flip-card.js"></script>
        <script src="../assets/tether/tether.min.js"></script>
        <script type="text/javascript" src="../assets/monjs/disponibilite.js"></script>
        <!-- Select 2 -->
        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                $('.listecommune').select2({
                    placeholder: "Code postal / Lieu",
                    allowClear: true
                });
            });
        </script>
        <!-- Sweetalert -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- Supprimer mot de passe -->
        <script type="text/javascript">
            $(function(){
                function SupprimerCompte(){
                    swal({
                        title: "Supprimer mon compte",
                        text: "Vous êtes sur le point de supprimer définitivement votre compte et vos annonces.\n\n\
                        Cliquez sur le bouton \"Supprimer\" pour confirmer votre demande.",
                        icon: "warning",
                        buttons: ["Annuler", "Supprimer"],
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            VerificationSupprimerCompte();
                        }
                    });
                }
                function VerificationSupprimerCompte(){
                    swal({
                        content: {
                            element: "input",
                            attributes: {
                                placeholder: "Entrer votre mot de passe",
                                type: "password",
                            },
                        },
                        title: "Vous nous quittez déjà ?",
                        text: "Veuillez saisir votre mot de passe pour supprimer votre compte.",
                        button: "Supprimer mon compte",
                    })
                    .then((mdp) => {
                        if (mdp) {
                            swal("Votre compte a bien été supprimer !", { icon: "success", });
                            $.post(
                                "config.php",{
                                    id_User : <?php echo $user['id_utilisateur']; ?>,
                                    motdepasse : mdp
                                },
                                function(data){
                                    var obj = JSON.parse(data)
                                    if (obj.supprime == "ok") {
                                        $('head').append('<meta http-equiv="refresh" content="2;URL=../config/deconnexion.php">');
                                    }else{
                                        swal("","Mot de passe incorrect !", 
                                            "warning", {
                                            button: "Réessayer",
                                        }).then((value) => {
                                            VerificationSupprimerCompte();
                                        });
                                    }
                                   
                                }
                            )
                        }
                    });
                }

                $('#supprimer-compte').click(SupprimerCompte)
            })
        </script>
        <!-- Changement de mot de passe  -->
        <?php if (isset($_GET['identifiants'])): ?>
            <script type="text/javascript">
                $(function(){
                    $('#pwd').change(function(){
                        var lemdp = $(this).val();
                        $.post(
                            "config.php",
                            {
                                pwd : lemdp
                            },
                            function(data){
                                var obj = JSON.parse(data)
                                $('#ancienMDP').attr("value",obj.lemdpasse) 
                            }
                        )
                    })

                    

                    $('#formMDP').submit(function(event){
                        var changer = true;
                        var mdp = $('#pwd'), mdp1 = $('#pwd1'), mdp2 = $('#confirm-pwd');
                        var msg_erreur_mdp = $('#erreur_mdp'), msg_erreur_mdp1 = $('#erreur_mdp1'), msg_erreur_mdp2 = $('#erreur_mdp2'), msg_erreur_mdpdifferents = $('#erreur_mdpdifferents')

                        // Le nombre de caractère de chaque champs
                        var caractere_mdp1 = mdp1.val().length, caractere_mdp2 = mdp2.val().length;


                        /*Si mot de passe ou confirmation de mot passe est < 6 caractères*/
                        if (caractere_mdp1<6 || caractere_mdp2<6) {
                            /*Si mot de passe 1*/
                            if (caractere_mdp1<6) {
                                msg_erreur_mdp1.empty().append("<ul class='mb-0'><li>Votre mot de passe doit conternir au moins 6 caractères !</li></ul>");
                                mdp1.css("border","1px solid red");
                            }else{
                                msg_erreur_mdp1.empty();
                            }

                            /*Si mot de passe 2*/
                            if (caractere_mdp2<6) {
                                msg_erreur_mdp2.empty().append("<ul class='mb-0'><li>Votre confirmation de mot de passe doit conternir au moins 6 caractères !</li></ul>");
                                mdp2.css("border","1px solid red");
                            }else{
                                msg_erreur_mdp2.empty();
                            }
                            changer = false;

                        }else{ 
                            msg_erreur_mdp1.empty();
                            msg_erreur_mdp2.empty();
                            mdp1.css("border-style","none");
                            mdp2.css("border-style","none");
                        }

                        /*Si les deux mots de passe sont différents*/
                        if (mdp1.val()!= mdp2.val()) {

                            msg_erreur_mdpdifferents.empty().append("<ul class='mb-0'><li>Le mot de passe est différent de sa confirmation !</li></ul>");
                            /*Ajouter des bordures alert*/
                            mdp1.css("border","1px solid red");
                            mdp2.css("border","1px solid red");
                            changer = false;
                        }else{
                            msg_erreur_mdpdifferents.empty()
                            mdp1.css("border-style","none");
                            mdp2.css("border-style","none");
                        }

                        /*Si ça n'existe pas*/
                        if ($('#ancienMDP').val() == "no_existe") {
                            msg_erreur_mdp.empty().append("<ul class='mb-0'><li>Le mot de passe est incorrect !</li></ul>");
                            mdp.css("border","1px solid red");
                            changer = false;
                        }else{
                            msg_erreur_mdp.empty()
                            mdp.css("border-style","none");
                        }

                        if (!changer) {
                            event.preventDefault();
                        }
                        
                    })
                })
            </script>
            <!-- Toogle mot de passe -->
            <script type="text/javascript">
                $(function(){
                    $("#button-addon").click(function(){
                        if ($("#iconCacherMDP").hasClass('fa-eye')) {
                            $(this).attr("title","Cacher le mot passe")
                            $("#iconCacherMDP").removeClass('fa-eye').addClass('fa-eye-slash')
                            $("#lepwd").attr("type","");
                        }else{
                            $(this).attr("title","Afficher le mot de passe")
                            $("#iconCacherMDP").removeClass('fa-eye-slash').addClass('fa-eye')
                            $("#lepwd").attr("type","password");
                        }
                    })
                    
                    $("#button-addon1").click(function(){
                        if ($("#iconCacherMDP1").hasClass('fa-eye')) {
                            $(this).attr("title","Cacher le mot passe")
                            $("#iconCacherMDP1").removeClass('fa-eye').addClass('fa-eye-slash')
                            $("#pwd").attr("type","");
                        }else{
                            $(this).attr("title","Afficher le mot de passe")
                            $("#iconCacherMDP1").removeClass('fa-eye-slash').addClass('fa-eye')
                            $("#pwd").attr("type","password");
                        }
                    })

                    $("#button-addon2").click(function(){
                        if ($("#iconCacherMDP2").hasClass('fa-eye')) {
                            $(this).attr("title","Cacher le mot passe")
                            $("#iconCacherMDP2").removeClass('fa-eye').addClass('fa-eye-slash')
                            $("#pwd1").attr("type","");
                        }else{
                            $(this).attr("title","Afficher le mot de passe")
                            $("#iconCacherMDP2").removeClass('fa-eye-slash').addClass('fa-eye')
                            $("#pwd1").attr("type","password");
                        }
                    })
                    
                    $("#button-addon3").click(function(){
                        if ($("#iconCacherMDP3").hasClass('fa-eye')) {
                            $(this).attr("title","Cacher le mot passe")
                            $("#iconCacherMDP3").removeClass('fa-eye').addClass('fa-eye-slash')
                            $("#confirm-pwd").attr("type","");
                        }else{
                            $(this).attr("title","Afficher le mot de passe")
                            $("#iconCacherMDP3").removeClass('fa-eye-slash').addClass('fa-eye')
                            $("#confirm-pwd").attr("type","password");
                        }
                    })
                })
            </script>
        <?php endif ?>
            
    </body>
</html>
