<?php 

	if (session_status() == PHP_SESSION_NONE) { session_start(); }

    require_once "../config/classes.php";

    $user = "";

    if ($_SESSION['id_utilisateur']) {

    	$user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
    	$lemdp = "no_existe";
    	if (isset($_POST['pwd'])) {
    		if (md5($_POST['pwd']) == $user['mdp']) {
    			$lemdp = "existe";
    		}
            echo '{"lemdpasse":"'.$lemdp.'"}';
    	}

        if (isset($_POST['id_User']) && isset($_POST['motdepasse'])) {
            $reponse = "";
            if ($user['mdp'] == md5($_POST['motdepasse'])) {
                $Utilisateur->deleteUtilisateur($_POST['id_User']);
                $reponse = "ok";
            }
            
            echo '{"supprime":"'.$reponse.'"}';
        }
    	
    }else{
        header("Location: ../");
    }

    //DELETE FROM `utilisateur` WHERE `utilisateur`.`id_utilisateur` = 17