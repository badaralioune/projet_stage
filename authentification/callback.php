<?php 
	require_once "../config/fonctions.php";
	require_once "config.php";

	try {
		$accessToken = $handler->getAccessToken();
	} catch (\Facebook\Exceptions\FacebookResponeException $e) {
		echo "Response Exception : " . $e->getMessage();
		exit();
	} catch (\Facebook\Exceptions\FacebookSDKException $e) {
		echo "SDK Exception : " . $e->getMessage();
		exit();
	}

	if (!$accessToken) {
		header("Location: login.php");
		exit();
	}


	$oAuth2Client = $FBObject->getOAuth2Client();
	if($accessToken->isLongLived()){
		$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
		$response = $FBObject->get("/me?fields=id,first_name,last_name,email,picture.type(large),gender", $accessToken);
		$tableauDataFB = $response->getGraphNode()->asArray();
		
		/*On déclare les variables pour les infos récupérer de l'utilisateur*/
		$id_utilisateur = $tableauDataFB['id'];
		$nom = $tableauDataFB['first_name'];
		$prenom = $tableauDataFB['last_name'];
		$mail = "";
		if (isset($tableauDataFB['email'])) {
			$mail = $tableauDataFB['email'];
		}
		$photo = $tableauDataFB['picture']['url'];
		$pseudo = $nom.$prenom;
		$civile = "";
		if ($tableauDataFB['gender'] == "male") {
			$civile = "m";
		}elseif ($tableauDataFB['gender'] == "female") {
			$civile = "f";
		}else{}
		
		/**/
		$idUser = $Utilisateur->getUtilisateurById($id_utilisateur);

		/*Si retourne null on insère dans la bdd*/
		if (empty($idUser['id_utilisateur'])) {
			$Utilisateur->setUtilisateurFB($id_utilisateur,$nom,$prenom,$mail,$photo,$pseudo,$civile);
			/*$Particulier->setParticulierFB($nom,$prenom,$mail,$photo,$pseudo,$civile);
			$idUser = $Utilisateur->getUtilisateurById($id_utilisateur);*/
			var_dump($id_utilisateur,$nom,$prenom,$mail,$photo,$pseudo,$civile); die();
		}
		

		// On démarre la session
		if (session_status() == PHP_SESSION_NONE) {
	        session_start();
	    }

	    $_SESSION['id_utilisateur'] = $idUser[];  


		$_SESSION['access_token'] = (string) $accessToken;
		header("Location: ../");
		exit();
 	}

?>