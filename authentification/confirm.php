<?php
    /*Démarrage de session*/
    session_start();
    /*Les tables (requetes)*/
    require_once "../config/classes.php";
    /*Les fonctions*/
    require_once "../config/fonctions.php";


?>
<!DOCTYPE html>
<html  >
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="../assets/images/logo2.png" type="image/x-icon">
        <meta name="description" content="">
        <?php if (isset($_POST['sinscrire'])): ?>
            <meta http-equiv="refresh" content="3;URL=../">
        <?php endif ?>
        <title>Confirmation de l'inscription</title>
        <link rel="stylesheet" href="../assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="../assets/socicon/css/styles.css">
        <link rel="stylesheet" href="../assets/dropdown/css/style.css">
        <link rel="stylesheet" href="../assets/as-pie-progress/css/progress.min.css">
        <link rel="stylesheet" href="../assets/tether/tether.min.css">
        <link rel="stylesheet" href="../assets/theme/css/style.css">
        <link rel="preload" as="style" href="../assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
        <style>
            .mon-overflow{
                height: 350px;
                overflow-y: scroll;
                overflow-x: hidden;
            }

            /*texte hr*/
            .mon-hr {
                /* centre verticalement les enfants entre eux */
                align-items: center;

                /* active flexbox */
                display: flex;

                /* garde le texte centré s’il passe sur plusieurs lignes ou si flexbox n’est pas supporté */
                text-align: center;
            }

            .mon-hr::before,
            .mon-hr::after {
                /* la couleur est volontairement absente ; ainsi elle sera celle du texte */
                border-top: .0625em solid;

                /* nécessaire pour afficher les pseudo-éléments */
                content: "";

                /* partage le reste de la largeur disponible */
                flex: 1;

                /* espace les traits du texte */
                margin: 0 .5em;
            }
        </style>
    </head>
    <body>
        <section class="menu cid-rVmtRueE1z" once="menu" id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="../"><img src="../assets/images/logo2.png" alt="Nom du site" style="height: 3.8rem;"></a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-4" href="../">NOM DU SITE</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../toutes_annonces/"><span class="mbri-bulleted-list mbr-iconfont mbr-iconfont-btn"></span>Toutes les demandes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../deposer_annonce/"><span class="mbri-edit mbr-iconfont mbr-iconfont-btn"></span>Déposer une annonce</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../profil/"><span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span>Mon profil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-black display-4" href="sign_up.php"><span class="mbri-plus mbr-iconfont mbr-iconfont-btn"></span>Inscription</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="login.php"><span class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>Connexion</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </section>
        <section class="header15 mt-5" id="header15-2">
            <div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(255, 255, 255);"></div>
            <div class="container align-center">
                <div class="row">
                    <div class="col-lg-12 col-md-12 mt-5">
                        <div class="row">
                            <?php 
                                /* si formulaire d'inscription envoyé*/
                                if (isset($_POST['sinscrire'])) {

                                    $statut =$_POST['statut'];
                                    $pseudo =$_POST['pseudo'];
                                    $mail =$_POST['email'];
                                    $tel =$_POST['tel'];
                                    $mdp1 =$_POST['pwd1'];
                                    $mdp2 =$_POST['pwd2'];
                                    $siret =""; 
                                    $raison_sociale="";
                            
                                    // print_r($_POST);
                                    $url = "https://www.google.com/recaptcha/api/siteverify";
                                    $data = [
                                        'secret' => "6LeQkfkUAAAAAJ78H8_7WkOsMCi8W3-0cBlsFCUb",
                                        'response' => $_POST['token'],
                                        // 'remoteip' => $_SERVER['REMOTE_ADDR']
                                    ];

                                    $options = array(
                                        'http' => array(
                                          'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                                          'method'  => 'POST',
                                          'content' => http_build_query($data)
                                        )
                                      );

                                    $context  = stream_context_create($options);
                                    $response = file_get_contents($url, false, $context);

                                    $res = json_decode($response, true);

                                    /*Si le siret existe et la raison sociale existe*/
                                    if (isset($_POST['siret']) && isset($_POST['raison_sociale'])) {
                                        $siret = $_POST['siret'];
                                        $raison_sociale = $_POST['raison_sociale'];
                                    }

                                    //var_dump($res); die();

                                    if($res['success'] == true) {
                                        
                                        // Perform you logic here for ex:- save you data to database
                                        // echo '<div class="alert alert-success text-center">
                                        //         <strong>Succès !</strong> Votre inscription a bien été prise en compte.
                                        //       </div>';
                                        /*Insertion dans la base et message de confirmation*/
                                        createUser($mail, $tel, $pseudo, md5($mdp1), $siret, $raison_sociale, $statut);
                                        /*On crée la variable session id_utilisateur*/
                                        $user = $Utilisateur->getUtilisateurByEmail($mail);
                                        $_SESSION['id_utilisateur'] = $user['id_utilisateur'];

                                    } else {
                                        echo '<div class="alert alert-warning text-center">
                                                  <strong>Erreur !</strong> Vous n\'êtes pas un humain.
                                              </div>';
                                    }
                                    
                                }
                            ?>
                        </div>
                                
                    </div>
                </div>
            </div>
        </section>

        <!-- Section footer -->
        <?php 
            require_once "../footer/footer_sous-racine.php";
        ?>
        <!-- Fin section footer -->
        <script src="../assets/web/assets/jquery/jquery.min.js"></script>
        <script src="../assets/popper/popper.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/smoothscroll/smooth-scroll.js"></script>
        <script src="../assets/dropdown/js/nav-dropdown.js"></script>
        <script src="../assets/dropdown/js/navbar-dropdown.js"></script>
        <script type="text/javascript" src="../assets/monjs/statut.js"></script>
    </body>
</html>
