<?php

	//start the session
	if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

	//include autoload file from vendor folder
	require_once("Facebook/autoload.php");
	


	$fb = new Facebook\Facebook([
	    'app_id' => '290681981945933', // replace your app_id
	    'app_secret' => '48ceec45c2e4db32cc93f0edd006bc4e',   // replace your app_scsret
	    'default_graph_version' => 'v2.10'
	        ]);


	$helper = $fb->getRedirectLoginHelper();
	$login_url = $helper->getLoginUrl("http://localhost/stage-projet/authentification/login.php");

	try {

	    $accessToken = $helper->getAccessToken();
	    if (isset($accessToken)) {
	        $_SESSION['access_token'] = (string) $accessToken;  //conver to string
	        //if session is set we can redirect to the user to any page 
	        header("Location: http://localhost/stage-projet/authentification/login.php");
	    }
	} catch (Exception $exc) {
	    echo $exc->getTraceAsString();
	}


	//now we will get users first name , email , last name
	if (isset($_SESSION['access_token'])) {

	    try {

	        $fb->setDefaultAccessToken($_SESSION['access_token']);
	        $res = $fb->get('/me?fields=id,first_name,last_name,email,picture.type(large),gender');
	        $userFacebook = $res->getGraphUser();
	       	$infosUser =  'Hello, '/*,$user->getField('name')*/.$userFacebook['first_name'].' '.$userFacebook['last_name'].'<br>'.$userFacebook['id'].'<br>'.$userFacebook['email'].'<br>'.$userFacebook['picture']['url'].'<br>'.$userFacebook['gender'].'<br>';
	        
	        /*On déclare les variables pour les infos récupérer de l'utilisateur*/
	        $id_utilisateur = $userFacebook['id'];
			$nom = $userFacebook['first_name'];
			$prenom = $userFacebook['last_name'];
			$mail = "";
			if (isset($userFacebook['email'])) {
				$mail = $userFacebook['email'];
			}
			$photo = $userFacebook['picture']['url'];
			$pseudo = $prenom.$id_utilisateur;
			$civile = "";
			if ($userFacebook['gender'] == "male") {
				$civile = "m";
			}elseif ($userFacebook['gender'] == "female") {
				$civile = "f";
			}else{}

			//var_dump($_SERVER['PHP_SELF'],$_SERVER['HTTP_REFERER'],$_SERVER['DOCUMENT_ROOT'], $_SERVER['SERVER_NAME'], $_SERVER['REQUEST_URI']); die();

			/*Si l'identifiant n'existe pas, on l'insère dans la base de données*/
			if(empty($Utilisateur->getUtilisateurById($id_utilisateur)['id_utilisateur'])){
				
				$Utilisateur->setUtilisateurFB($id_utilisateur,$nom,$prenom,$mail,$photo,$pseudo,$civile);
				$Particulier->setParticulierFB($id_utilisateur,$nom,$prenom,$mail,$photo,$pseudo,$civile);
				$idUser = $Utilisateur->getUtilisateurById($id_utilisateur);
				
				
				/*Si première connexion, vous allez être rediriger vers la page de profil*/
				if (substr($_SERVER['HTTP_REFERER'], -9) == 'login.php') {
					$_SESSION['id_utilisateur'] = $idUser['id_utilisateur'];
					header("Location: ../profil/");
				}
				
				/*Si première inscription, vous allez être rediriger vers la page de finalisation de compte*/
				if (substr($_SERVER['HTTP_REFERER'], -11) == 'sign_up.php') {
					$_SESSION['id_utilisateur'] = $idUser['id_utilisateur']; 
					$finalisation_compte = "../finalisation_compte/?user_id=".md5($idUser['id_utilisateur'])."&".md5('compteValide')."=";
					header("Location: ".$finalisation_compte);
				}
				
			}else{
				$idUser = $Utilisateur->getUtilisateurById($id_utilisateur)['id_utilisateur'];
				$_SESSION['id_utilisateur'] = $idUser; 
				header("Location: ../");
			}

			
	        
	    } catch (Exception $exc) {
	        echo $exc->getTraceAsString();
	    }
	}
