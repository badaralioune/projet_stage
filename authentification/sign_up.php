<?php 
    require_once "../config/classes.php";
    /*Sessions*/
    $connexion = false;
    $user = [];
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        if (isset($_SESSION['id_utilisateur'])) {
            $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
            $connexion = true;
            header("Location:../");
        }
    }

    require_once "config.php";
?>
<!DOCTYPE html>
<html  lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="../assets/images/logo2.png" type="image/x-icon">
        <meta name="description" content="">
        <title>Inscription</title>
        <link rel="stylesheet" href="../assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="../assets/socicon/css/styles.css">
        <link rel="stylesheet" href="../assets/dropdown/css/style.css">
        <link rel="stylesheet" href="../assets/as-pie-progress/css/progress.min.css">
        <link rel="stylesheet" href="../assets/tether/tether.min.css">
        <link rel="stylesheet" href="../assets/theme/css/style.css">
        <link rel="preload" as="style" href="../assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <style>
            .mon-overflow{
                height: 350px;
                overflow-y: scroll;
                overflow-x: hidden;
            }

            /*texte hr*/
            .mon-hr {
                /* centre verticalement les enfants entre eux */
                align-items: center;

                /* active flexbox */
                display: flex;

                /* garde le texte centré s’il passe sur plusieurs lignes ou si flexbox n’est pas supporté */
                text-align: center;
            }

            .mon-hr::before,
            .mon-hr::after {
                /* la couleur est volontairement absente ; ainsi elle sera celle du texte */
                border-top: .0625em solid;

                /* nécessaire pour afficher les pseudo-éléments */
                content: "";

                /* partage le reste de la largeur disponible */
                flex: 1;

                /* espace les traits du texte */
                margin: 0 .5em;
            }
            /*Bouuton se connecter*/
            .mbr-form .input-group-btn button[type="submit"] {
                border-radius: 100px !important;
                padding: 1rem 3rem;
            }
        </style>
    </head>
    <body>
        <section class="menu cid-rVmtRueE1z" id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="../"><img src="../assets/images/logo2.png" alt="Nom du site" style="height: 3.8rem;"></a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-4" href="../">NOM DU SITE</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../#features1-8"><span class="mbri-bulleted-list mbr-iconfont mbr-iconfont-btn"></span>Toutes les demandes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../deposer_annonce/"><span class="mbri-edit mbr-iconfont mbr-iconfont-btn"></span>Déposer une annonce</a>
                        </li>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../profil/">
                                    <span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span> <?php echo $user['pseudo']; ?>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item">
                                <a class="nav-link link text-black display-4" href="sign_up.php"><span class="mbri-plus mbr-iconfont mbr-iconfont-btn"></span>Inscription</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="login.php"><span class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>Connexion</a>
                            </li>   
                        <?php endif ?>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../config/deconnexion.php"><span class="mbri-logout mbr-iconfont mbr-iconfont-btn"></span>Déconnexion</a>
                            </li>
                        <?php endif ?>
                    </ul>
                </div>
            </nav>
        </section>
        <section class="header15 cid-rVmx6gBoE2 mbr-parallax-background pb-1" id="header15-2">
            <div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(255, 255, 255);"></div>
            <div class="container align-center">
                <div class="col-lg-12 col-md-12 mt-1">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <div class="form-container p-md-2 p-sm-2 rounded">
                                <div class="media-container-column" data-form-type="formoid">
                                    <h4>Rejoignez-nous vite !</h4>
                                    <div class="col-md-12 input-group-btn mt-2 mb-1">
                                        <a class="btn btn-md btn-primary display-4 p-1" href="<?php echo $login_url; ?>"><span class="socicon socicon-facebook mbr-iconfont mbr-iconfont-btn"></span>Créer un compte avec Facebook</a>
                                    </div>
                                    <h4 class="mon-hr">ou</h4>
                                    <!---Formbuilder Form--->
                                    <div class="col-md-12 border-bottom">
                                        
                                        <div class="alert alert-danger p-0 m-0" role="alert"><small id="mdp_non_identiques" class="text-white"></small></div>
                                    
                                        <form action="confirm.php" method="POST" class="mbr-form form-with-styler mt-2" id="formulaireIns">
                                            <!-- <div class="form-group row text-left">
                                                <div class="col-md-6">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="radio1" id="situation1" value="value1" required>
                                                        <label class="form-check-label" for="situation1">Vous êtes à la recherche d'un service</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="radio1" id="situation2" value="value2" required>
                                                        <label class="form-check-label" for="situation2">Vous proposez un service</label>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- Statut -->
                                            <!-- <div class="form-group row text-left mb-1">
                                                <label for="statut" class="col-sm-6 col-form-label">Votre * :</label>
                                                <div class="col-sm-6">
                                                    <select name="statut" class="form-control form-control-sm champ0 " id="statut" required>
                                                        <option value="" selected disabled>-- Statut --</option>
                                                        <option value="particulier">Particulier</option>
                                                        <?php foreach ($lesTypeEtablissements as $etablissement): ?>
                                                            <option value="<?php echo $etablissement['code_type_etablissement']; ?>"><?php echo $etablissement['lib_etablissement']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <div class="alert alert-danger p-0 m-0" role="alert"><small class="text-white verif0"></small></div>
                                                </div>
                                            </div> -->
                                            <input type="hidden" value="particulier" name="statut">
                                            <!-- Siret et raison social -->
                                            <!-- <div id="blocsiret" class="text-left mb-1">
                                                <div class="form-group row ">
                                                    <label for="siret" class="col-sm-6 col-form-label">N° SIRET :</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="siret" value="" class="form-control form-control-sm champ1 p-2" id="siret"  placeholder="Numéro du siret" required>
                                                        <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_siret" class="text-white verif1"></small></div>
                                                    </div>
                                                </div>
                                                <div class="form-group row  mb-1">
                                                    <label for="raison_sociale" class="col-sm-6 col-form-label">Raison sociale :</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="raison_sociale" class="form-control form-control-sm champ2 p-2" id="raison_sociale" value="<?php if(isset($_GET['raison_sociale'])){echo $_GET['raison_sociale'];} ?>" placeholder="Raison sociale" required>
                                                        <div class="alert alert-danger p-0 m-0" role="alert"><small class="text-white verif2"></small></div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- pseudo -->
                                            <div class="form-group row text-left mb-1">
                                                <label for="pseudo" class="col-sm-6 col-form-label">Nom utilisateur * :</label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="pseudo" value="" class="form-control form-control-sm champ3 p-2" id="pseudo"  placeholder="pseudo" required>
                                                    <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_pseudo" class="text-white verif3"></small></div>
                                                </div>
                                            </div>
                                            <!-- Adresse email -->
                                            <div class="form-group row text-left mb-1">
                                                <label for="email" class="col-sm-6 col-form-label">Votre adresse e-mail * :</label>
                                                <div class="col-sm-6">
                                                    <input type="email" name="email" value="" class="form-control form-control-sm champ4 p-2" id="email" placeholder="azerty@gmail.com" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
                                                    <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_email" class="text-white verif4"></small></div>
                                                </div>
                                            </div>
                                            <!-- Bouton continuer inscription -->
                                            <!-- <div class="row" id="btnblocInscription1">
                                                <div class="col-md-12 input-group-btn">
                                                    <a class="btn btn-form btn-info display-4 p-2" id="continuerInscription">Continuer la création de compte</a>
                                                </div>
                                            </div> -->
                                            <!-- bloc partie 2 de l'inscripion -->
                                            <div id="blocInscription2">
                                                <!-- Téléphone -->
                                                <!-- <div class="form-group row text-left mb-1">
                                                    <label for="tel" class="col-sm-6 col-form-label">Votre numéro de téléphone * :</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="tel" class="form-control form-control-sm p-2" id="tel"  placeholder="Téléphone" required>
                                                        <div class="alert alert-danger p-0 m-0" role="alert"><small class="text-white verif5"></small></div>
                                                    </div>
                                                </div> -->
                                                <input type="hidden" name="tel" value="0">
                                                <!-- Mot de passe  -->
                                                <div class="form-group row text-left mb-1">
                                                    <label for="pwd1" class="col-sm-6 col-form-label">Votre mot de passe * :</label>
                                                    <div class="input-group mb-2 col-sm-6 ">
                                                        <input type="password" name="pwd1" class="form-control form-control-sm p-2" id="pwd1" placeholder="Mot de passe" required aria-describedby="button-addon2">
                                                        <div class="input-group-append">
                                                            <button class="btn btn-primary py-0 px-2 m-0 rounded-right" title="Afficher le mot passe" type="button" id="button-addon2"><i id="iconCacherMDP" class="fa fa-eye"></i></button>
                                                        </div>
                                                        <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_mdp1" class="text-white verif6"></small></div>
                                                    </div>
                                                </div>
                                                <!-- Confirmation Mot de passe -->
                                                <div class="form-group row text-left mb-1">
                                                    <label for="pwd2" class="col-sm-6 col-form-label">Confirmation le mot de passe * :</label>
                                                    <div class="input-group mb-2 col-sm-6 ">
                                                        <input type="password" name="pwd2" class="form-control form-control-sm p-2" id="pwd2" placeholder="Confirmation de mot de passe" required aria-describedby="button-addon3">
                                                        <div class="input-group-append">
                                                            <button class="btn btn-primary py-0 px-2 m-0 rounded-right" title="Afficher le mot passe" type="button" id="button-addon3"><i id="iconCacherMDP1" class="fa fa-eye"></i></button>
                                                        </div>
                                                        <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_mdp2" class="text-white"></small></div>
                                                        <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_mdpdifferents" class="text-white verif7"></small></div>
                                                    </div>
                                                </div>
                                                <!-- Validation des conditions générales dutilisation -->
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input p-2" id="validationConditionGenerale" required>
                                                    <label class="form-check-label" for="validationConditionGenerale"><small>J'accepte les <a href="#"> Conditions générales d'utilisation. </a></small></label>
                                                    <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_validerCondition" class="text-white"></small></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 input-group-btn">
                                                        <input type="hidden" id="verifBddPseudo">
                                                        <input type="hidden" id="verifBddEmail">
                                                        <input type="hidden" id="verifBddSiret">
                                                        <input type="hidden" id="token" name="token">
                                                        <button type="submit"  name="sinscrire" id="sinscrire" class="btn btn-form btn-info display-4">Créer un compte</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-12">
                                        <p><small>Vous avez déjà créer un compte ?</small> <a href="login.php" class="btn btn-secondary form-control-sm">Se connecter</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                </div>
            </div>
        </section>

        <!-- Section footer -->
        <?php 
            require_once "../footer/footer_sous-racine.php";
        ?>
        <!-- Fin section footer -->
        <script src="../assets/web/assets/jquery/jquery.min.js"></script>
        <script src="../assets/popper/popper.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/smoothscroll/smooth-scroll.js"></script>
        <script src="../assets/dropdown/js/nav-dropdown.js"></script>
        <script src="../assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="../assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="../assets/parallax/jarallax.min.js"></script>
        <script src="../assets/mbr-flip-card/mbr-flip-card.js"></script>
        <script src="../assets/tether/tether.min.js"></script>
        <script type="text/javascript">

            $(function(){
                /*Les champs*/
                var /*statut = $('#statut'), siret = $('#siret'), raison_sociale = $('#raison_sociale'),*/ pseudo = $('#pseudo'), email = $('#email'), /*tel = $('#tel'),*/ mdp1 = $('#pwd1'), mdp2 = $('#pwd2');

                /*Tableau permettant de vérifier les champs*/
                var champs = [/*statut, siret, raison_sociale, */pseudo, email, /*tel,*/ mdp1, mdp2];

                /*On récupère les données de la BDD si champ change*/
                for (var i = 0 ; i < champs.length; i++) {
                    champs[i].on("change",function(){
                        
                        $.get( 
                            "verif_donneesInscription.php",{
                                lepseudo : pseudo.val(),
                                lemail : email.val(),
                                /*lesiret : siret.val()*/
                            }, 
                            function(donnees) {
                                /*On récupère les données en JSON*/
                                var obj = JSON.parse(donnees);
                                
                                /*les champs hidden pour insérer les données de la BDD*/
                                var verifBddPseudo = $("#verifBddPseudo"), verifBddEmail = $("#verifBddEmail")/*, verifBddSiret = $("#verifBddSiret")*/;

                                /*On insère les données*/
                                verifBddPseudo.attr("value",obj.unpseudo);
                                verifBddEmail.attr("value",obj.unemail);
                                // verifBddSiret.attr("value",obj.unsiret);
                            }
                        ); 
                    });
                } 

                /*Formulaire d'envoie*/
                $("#formulaireIns").on("submit",function(event){
                    /*On récupère les données données des champs pour les données de la BDD*/
                    var verifBddPseudo = $("#verifBddPseudo").val(), verifBddEmail = $("#verifBddEmail").val()/*, verifBddSiret = $("#verifBddSiret").val()*/;
                    /*Les variables des messages erreurs*/
                    var msg_erreur_mdp1 = $('#erreur_mdp1'), msg_erreur_mdp2 = $('#erreur_mdp2'), msg_erreur_mdpdifferents = $('#erreur_mdpdifferents')/*, msg_erreur_siret = $('#erreur_siret')*/, msg_erreur_pseudo = $('#erreur_pseudo'), msg_erreur_email= $('#erreur_email'), msg_validation = $("#erreur_validerCondition");
                    /*Variable qui vérifie les données des champs*/
                    var verifchamp = true;
                    // Le nombre de caractère de chaque champs
                    var caractere_mdp1 = mdp1.val().length, caractere_mdp2 = mdp2.val().length;
                    
                    /*On parcourt les champs et on enleve les bordures en rouge et les messages d'erreur*/
                    for (var i = 0 ; i < champs.length; i++) {
                        champs[i].css("border-style","none");
                        $('.verif'+i).empty();
                    }


                    /*Si mot de passe ou confirmation de mot passe est < 6 caractères*/
                    if (caractere_mdp1<6 || caractere_mdp2<6) {
                        /*Si mot de passe 1*/
                        if (caractere_mdp1<6) {
                            msg_erreur_mdp1.empty().append("<ul class='mb-0'><li>Votre mot de passe doit conternir au moins 6 caractères !</li></ul>");
                            mdp1.css("border","1px solid red");
                        }else{
                            msg_erreur_mdp1.empty();
                        }

                        /*Si mot de passe 2*/
                        if (caractere_mdp2<6) {
                            msg_erreur_mdp2.empty().append("<ul class='mb-0'><li>Votre confirmation de mot de passe doit conternir au moins 6 caractères !</li></ul>");
                            mdp2.css("border","1px solid red");
                        }else{
                            msg_erreur_mdp2.empty();
                        }
                        verifchamp = false;

                    }else{ 
                        msg_erreur_mdp1.empty();
                        msg_erreur_mdp2.empty();
                        mdp1.css("border-style","none");
                        mdp2.css("border-style","none");
                    }

                    /*Si les deux mots de passe sont différents*/
                    if (mdp1.val()!= mdp2.val()) {

                        msg_erreur_mdpdifferents.empty().append("<ul class='mb-0'><li>Le mot de passe est différent de sa confirmation !</li></ul>");
                        /*Ajouter des bordures alert*/
                        mdp1.css("border","1px solid red");
                        mdp2.css("border","1px solid red");
                        verifchamp = false;
                    }else{
                        msg_erreur_mdpdifferents.empty()
                        mdp1.css("border-style","none");
                        mdp2.css("border-style","none");
                    }

                    /*On vérifie les champs*/
                    for (var i = 0 ; i < champs.length; i++) {
                        if (champs[i].attr("required") && champs[i].val() == " ") {
                            champs[i].css("border","1px solid red");
                            $('.verif'+i).empty().append("<ul class='mb-0'><li>Veuillez renseigner ce champ.</li></ul>")
                            verifchamp = false;
                        }
                    }

                    /*Si le pseudo est existant*/
                    if (verifBddPseudo != "") {
                        msg_erreur_pseudo.empty().append("<ul class='mb-0'><li>Votre pseudo est déjà associé à un autre compte !</li></ul>")
                        pseudo.css("border","1px solid red");
                        verifchamp = false;
                    }

                    /*Si l'email est existant*/
                    if (verifBddEmail != "") {
                        msg_erreur_email.empty().append("<ul class='mb-0'><li>Votre adresse e-mail est déjà associée à un autre compte !</li></ul>")
                        email.css("border","1px solid red");
                        verifchamp = false;
                    }

                    /*Si le n°SIRET est existant*/
                    /*if (verifBddSiret != "") {
                        msg_erreur_siret.empty().append("<ul class='mb-0'><li>Votre numéro de SIRET est déjà associée à un autre compte !</li></ul>")
                        siret.css("border","1px solid red");
                        verifchamp = false;
                    }*/

                    /*S'il y a une erreur on affiche*/
                    if (verifchamp == false) {
                        event.preventDefault();
                    }
                    
                })
            })
        </script>
        <!-- Toogle mot de passe -->
        <script type="text/javascript">
            $(function(){
                $("#button-addon2").click(function(){
                    if ($("#iconCacherMDP").hasClass('fa-eye')) {
                        $(this).attr("title","Cacher le mot passe")
                        $("#iconCacherMDP").removeClass('fa-eye').addClass('fa-eye-slash')
                        $("#pwd1").attr("type","");
                    }else{
                        $(this).attr("title","Afficher le mot de passe")
                        $("#iconCacherMDP").removeClass('fa-eye-slash').addClass('fa-eye')
                        $("#pwd1").attr("type","password");
                    }
                })
                $("#button-addon3").click(function(){
                    if ($("#iconCacherMDP1").hasClass('fa-eye')) {
                        $(this).attr("title","Cacher le mot passe")
                        $("#iconCacherMDP1").removeClass('fa-eye').addClass('fa-eye-slash')
                        $("#pwd2").attr("type","");
                    }else{
                        $(this).attr("title","Afficher le mot de passe")
                        $("#iconCacherMDP1").removeClass('fa-eye-slash').addClass('fa-eye')
                        $("#pwd2").attr("type","password");
                    }
                })
            })
        </script>
        <!-- recaptcha -->
        <script src="https://www.google.com/recaptcha/api.js?render=6LeQkfkUAAAAAGZU_JL0b_DodV8_85fiQFStU4xW"></script>
        <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('6LeQkfkUAAAAAGZU_JL0b_DodV8_85fiQFStU4xW', {action: 'homepage'}).then(function(token) {
                $('#token').attr("value",token);
            });
        });
        </script>
    </body>
</html>
