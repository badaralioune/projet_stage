<?php 
    require_once "../config/classes.php";
    /*Sessions*/
    $connexion = false;
    $user = [];
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        if (isset($_SESSION['id_utilisateur'])) {
            $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
            $connexion = true;
            header("Location:../");
        }
    }

    require_once "config.php";
?>


<!DOCTYPE html>
<html  lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="../assets/images/logo2.png" type="image/x-icon">
        <meta name="description" content="">
        <title>Connexion</title>
        <link rel="stylesheet" href="../assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="../assets/socicon/css/styles.css">
        <link rel="stylesheet" href="../assets/dropdown/css/style.css">
        <link rel="stylesheet" href="../assets/as-pie-progress/css/progress.min.css">
        <link rel="stylesheet" href="../assets/tether/tether.min.css">
        <link rel="stylesheet" href="../assets/theme/css/style.css">
        <link rel="preload" as="style" href="../assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <style>
            .mon-overflow{
                height: 350px;
                overflow-y: scroll;
                overflow-x: hidden;
            }

            /*texte hr*/
            .mon-hr {
                /* centre verticalement les enfants entre eux */
                align-items: center;

                /* active flexbox */
                display: flex;

                /* garde le texte centré s’il passe sur plusieurs lignes ou si flexbox n’est pas supporté */
                text-align: center;
            }

            .mon-hr::before,
            .mon-hr::after {
                /* la couleur est volontairement absente ; ainsi elle sera celle du texte */
                border-top: .0625em solid;

                /* nécessaire pour afficher les pseudo-éléments */
                content: "";

                /* partage le reste de la largeur disponible */
                flex: 1;

                /* espace les traits du texte */
                margin: 0 .5em;
            }

            /*Bouuton se connecter*/
            .mbr-form .input-group-btn button[type="button"] {
                border-radius: 100px !important;
                padding: 1rem 3rem;
            }
        </style>
    </head>
    <body>
        <section class="menu cid-rVmtRueE1z"  id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="../"><img src="../assets/images/logo2.png" alt="Nom du site" style="height: 3.8rem;"></a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-4" href="../">NOM DU SITE</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../#features1-8"><span class="mbri-bulleted-list mbr-iconfont mbr-iconfont-btn"></span>Toutes les demandes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../deposer_annonce/"><span class="mbri-edit mbr-iconfont mbr-iconfont-btn"></span>Déposer une annonce</a>
                        </li>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../profil/">
                                    <span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span> <?php echo $user['pseudo']; ?>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="sign_up.php"><span class="mbri-plus mbr-iconfont mbr-iconfont-btn"></span>Inscription</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-black display-4" href="login.php"><span class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>Connexion</a>
                            </li>   
                        <?php endif ?>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../config/deconnexion.php"><span class="mbri-logout mbr-iconfont mbr-iconfont-btn"></span>Déconnexion</a>
                            </li>
                        <?php endif ?>
                    </ul>
                </div>
            </nav>
        </section>
        <section class="header15 cid-rVmx6gBoE2 mbr-parallax-background pb-1" id="header15-2">
            <div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(255, 255, 255);"></div>
            <div class="container align-center">
                <div class="col-lg-12 col-md-12 mt-1">
                    <div class="row">
                        <div class="col-md-6 offset-md-3 ">
                            <div class="form-container p-md-2 p-sm-2 rounded">
                                <div class="media-container-column" data-form-type="formoid">
                                    <h4>Connectez-vous !</h4>
                                    <div class="col-md-12 input-group-btn mt-2 mb-1">
                                        <a class="btn btn-md btn-primary display-4 p-1" href="<?php echo $login_url; ?>"><span class="socicon socicon-facebook mbr-iconfont mbr-iconfont-btn"></span>S'identifier avec Facebook</a>
                                    </div>
                                    <h4 class="mon-hr my-1">ou</h4>
                                    <!---Formbuilder Form--->
                                    <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreurConnexion" class="text-white"></small></div>
                                    <div class="col-md-12 border-bottom text-left">
                                        <form action="#" method="POST" class="mbr-form form-with-styler mt-2" id="formulaireConnexion">
                                            <!-- pseudo -->
                                            <div class="form-group row mb-1">
                                                <label for="mail" class="col-sm-12 col-form-label">Adresse e-mail :</label>
                                                <div class="col-sm-12">
                                                    <input type="email" name="mail" class="form-control p-2" id="mail" placeholder="Adresse e-mail" required>
                                                    <div class="alert alert-danger p-0 m-0" role="alert"><small id="erreur_mail" class="text-white"></small></div>
                                                </div>
                                            </div>
                                            <!-- Mot de passe  -->
                                            <div class="form-group row ">
                                                <label for="pwd" class="col-sm-12 col-form-label">Mot de passe :</label>
                                                <div class="input-group col-sm-12">
                                                    <input type="password" name="pwd" class="form-control p-2" id="pwd" placeholder="Mot de passe" required aria-describedby="button-addon1">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-primary py-0 px-2 m-0 rounded-right" title="Afficher le mot passe" type="button" id="button-addon1"><i id="iconCacherMDP" class="fa fa-eye"></i></button>
                                                    </div>
                                                    
                                                </div>
                                                <div class="alert alert-danger col-sm-12 p-0 m-0" role="alert"><small id="erreur_pwd" class="text-white"></small></div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 input-group-btn align-center">
                                                    <button type="button" id="seconnecter" class="btn btn-form btn-info display-4">Se connecter</button>
                                                </div>
                                                <div class="col-md-12 align-right">
                                                    <p><a href="#" id="mdp_oublie">Mot de passe oublié ?</a></p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-12">
                                        <p><small>Vous n'avez pas de compte ?</small> <a href="sign_up.php" class="btn btn-secondary form-control-sm">S'inscrire</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                </div>
            </div>
        </section>
        <!-- Section footer -->
        <?php 
            require_once "../footer/footer_sous-racine.php";
        ?>
        <!-- Fin section footer -->
        <script src="../assets/web/assets/jquery/jquery.min.js"></script>
        <script src="../assets/popper/popper.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/smoothscroll/smooth-scroll.js"></script>
        <script src="../assets/dropdown/js/nav-dropdown.js"></script>
        <script src="../assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="../assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="../assets/parallax/jarallax.min.js"></script>
        <script src="../assets/as-pie-progress/jquery-as-pie-progress.min.js"></script>
        <script src="../assets/mbr-flip-card/mbr-flip-card.js"></script>
        <script src="../assets/tether/tether.min.js"></script>
        <!-- SweetAlert -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="../assets/monjs/verif_formulaireConnexion.js"></script>
        <!-- <script type="text/javascript" src="../assets/monjs/mot_passe_oublie.js"></script> -->
        <!-- Toogle mot de passe -->
        <script type="text/javascript">
            $(function(){
                $("#button-addon1").click(function(){
                    if ($("#iconCacherMDP").hasClass('fa-eye')) {
                        $(this).attr("title","Cacher le mot passe")
                        $("#iconCacherMDP").removeClass('fa-eye').addClass('fa-eye-slash')
                        $("#pwd").attr("type","");
                    }else{
                        $(this).attr("title","Afficher le mot de passe")
                        $("#iconCacherMDP").removeClass('fa-eye-slash').addClass('fa-eye')
                        $("#pwd").attr("type","password");
                    }
                })
            })
        </script>
        <script type="text/javascript">
            $(function(){
    var btn_mdpoublie = $('#mdp_oublie');

    btn_mdpoublie.click(function(){
        swal({
          content: "input",
          title: "Récupération de votre mot de passe",
          text: "Pour demander un nouveau mot de passe, merci de renseigner votre adresse e-mail ci-dessous. Un e-mail avec un nouveau mot de passe vous sera envoyé.",
          button: "Réinitialiser mon mot de passe",
        })
        .then((value) => {
            if (!value) {
                
            }else{
                

                $.get(
                    "mdp_oublie.php",
                    {
                        lemail : value
                    },
                    function(data){
                        var reponse = JSON.parse(data);
                        
                        if (reponse.resultat == "ok") {
                            swal("","Le compte associé à cette adresse est introuvable.", 
                                "warning", {
                                button: "Réessayer",
                            });
                            
                        }else{
                            swal("","Un email vient de vous être envoyé pour vous permettre de réinitialiser votre mot de passe.\n\nSi vous ne l'avez pas reçu di'ci quelques minutes, pensez à vérifier qu'il ne se trouve pas dans vos courriers indésirables.", 
                                "success", {
                                button: "Ok",
                            });
                        }
                    }
                );
            }

    
        });
    })

})  
        </script>
    </body>
</html>
