<?php 
    require_once "../config/classes.php";
    /*Sessions*/
    $connexion = false;
    $user = [];
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        if (isset($_SESSION['id_utilisateur'])) {
            $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
            $connexion = true;
        }else{
            header("Location: ../");
        }
    }
?>
<!DOCTYPE html>
<html  >
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="../assets/images/logo2.png" type="image/x-icon">
        <meta name="description" content="">
        <title>Finalisation de compte</title>
        <link rel="stylesheet" href="../assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="../assets/socicon/css/styles.css">
        <link rel="stylesheet" href="../assets/dropdown/css/style.css">
        <link rel="stylesheet" href="../assets/as-pie-progress/css/progress.min.css">
        <link rel="stylesheet" href="../assets/tether/tether.min.css">
        <link rel="stylesheet" href="../assets/theme/css/style.css">
        <link rel="preload" as="style" href="../assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
        <!-- select 2 -->
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
        <style>
            .mon-overflow{
                height: 350px;
                overflow-y: scroll;
                overflow-x: hidden;
            }
            /*texte hr*/
            .mon-hr {
                /* centre verticalement les enfants entre eux */
                align-items: center;

                /* active flexbox */
                display: flex;

                /* garde le texte centré s’il passe sur plusieurs lignes ou si flexbox n’est pas supporté */
                text-align: center;
            }

            .mon-hr::before,
            .mon-hr::after {
                /* la couleur est volontairement absente ; ainsi elle sera celle du texte */
                border-top: .0625em solid;

                /* nécessaire pour afficher les pseudo-éléments */
                content: "";

                /* partage le reste de la largeur disponible */
                flex: 1;

                /* espace les traits du texte */
                margin: 0 .5em;
            }
            .cid-rVmE73Fmjs select.form-control, .cid-rVmE73Fmjs input.form-control{
              min-height: 10px;
              padding: 3px;
            }

            .cid-rVmE73Fmjs textarea.form-control{
              min-height: 152px;
            }   

            li.active{
                background-color: #000000;
            }

            /*.pagination-block{
                background-color: #000000;
            }*/         
        </style>
    </head>
    <body>
        <section class="menu cid-rVmtRueE1z" once="menu" id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="../"><img src="../assets/images/logo2.png" alt="Nom du site" style="height: 3.8rem;"></a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-4" href="../">NOM DU SITE</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../toutes_annonces/#features1-8"><span class="mbri-bulleted-list mbr-iconfont mbr-iconfont-btn"></span>Toutes les demandes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../deposer_annonce/"><span class="mbri-edit mbr-iconfont mbr-iconfont-btn"></span>Déposer une annonce</a>
                        </li>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-black display-4" href="../profil/">
                                    <span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span> <?php echo $user['pseudo']; ?>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../authentification/sign_up.php"><span class="mbri-plus mbr-iconfont mbr-iconfont-btn"></span>Inscription</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../authentification/login.php"><span class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>Connexion</a>
                            </li>   
                        <?php endif ?>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../config/deconnexion.php"><span class="mbri-logout mbr-iconfont mbr-iconfont-btn"></span>Déconnexion</a>
                            </li>
                        <?php endif ?>
                    </ul>
                </div>
            </nav>
        </section>
        <section class="engine">
            <a href="#">website maker</a>
        </section>

        <section class="features1 cid-rVmE73Fmjs" id="depot_annonce">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 mt-5">
                        <div class="form-container">
                            <div class="media-container-column" data-form-type="formoid">
                                <h1 class="mon-hr">Finalisation de votre compte</h1>
                                <p>
                                    Présentez-vous à la communauté des membres et multipliez les chances d'être contacté !
                                </p>


                                <?php if (isset($_GET['user_id']) && $_GET['user_id'] == md5($user['id_utilisateur']) && $_GET[md5('compteValide')]==""): ?>
                                    <div id="demo" class="carousel slide p-0" data-ride="carousel">
                                      
                                        <!-- The slideshow -->
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <div class="col-md-8 offset-md-2 text-left mt-5">
                                                    <form action="#">
                                                        <div class="card border bg-light">
                                                            <h5 class="card-header">1/4 - Quelle est votre situation actuelle ?</h5>
                                                            <div class="card-body">
                                                                <?php foreach ($lesSituations as $situation): ?>
                                                                    <div class="form-check mb-3">
                                                                        <input class="form-check-input" type="radio" name="situation" id="<?php echo $situation['id_situation']; ?>" value="<?php echo $situation['id_situation']; ?>"  required>
                                                                        <label class="form-check-label" for="<?php echo $situation['id_situation']; ?>">
                                                                            <?php echo $situation['lib_situation']; ?>
                                                                        </label>
                                                                    </div>
                                                                <?php endforeach ?>

                                                                <div class="my-4 align-center">
                                                                    <button type="submit" data-target="#demo" data-slide-to="1" id="etape1" class="btn btn-primary p-2">Enregistrer et continuer</button>
                                                                </div>

                                                                <div class="text-center my-2">
                                                                    <small>Complétez votre profil plus tard. <a href="../">Retour à la page d'accueil</a></small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="col-md-8 offset-md-2 text-left mt-5">
                                                    <form action="#">
                                                        <br>
                                                        <div class="card border">
                                                            <h5 class="card-header">2/4 - Informations personnelles</h5>
                                                            <div class="card-body">
                                                                        
                                                                <div class="form-group row">
                                                                    <label for="sexe" class="col-sm-4 col-md-4 col-form-label">Sexe :</label>
                                                                    <div class="col-sm-8 col-md-8" id="sexe">
                                                                        <div class="form-check mb-3">
                                                                            <input class="form-check-input civilite" type="radio" name="sexe" id="monsieur" value="m" <?php if($user['civilite'] == "m"){echo "checked";} ?> required>
                                                                            <label class="form-check-label" for="monsieur">
                                                                                Monsieur
                                                                            </label>
                                                                        </div>
                                                                        <div class="form-check mb-1">
                                                                            <input class="form-check-input civilite" type="radio" name="sexe" id="madame" value="f" <?php if($user['civilite'] == "f"){echo "checked";} ?> required>
                                                                            <label class="form-check-label" for="madame">
                                                                                Madame
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="nom" class="col-sm-4 col-md-4 col-form-label">Nom :</label>
                                                                    <div class="col-sm-8 col-md-8">
                                                                        <input type="text" class="form-control" id="nom" placeholder="Nom" value="<?php echo $user['nom']; ?>">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="prenom" class="col-sm-4 col-md-4 col-form-label">Prénom :</label>
                                                                    <div class="col-sm-8 col-md-8">
                                                                        <input type="text" class="form-control" id="prenom" placeholder="Prénom" value="<?php echo $user['prenom']; ?>">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="date_naissance" class="col-sm-4 col-md-4 col-form-label">Date de naissance :</label>
                                                                    <div class="col-sm-8 col-md-8">
                                                                        <input type="date" class="form-control" id="date_naissance" >
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="tel" class="col-sm-4 col-md-4 col-form-label">Téléphone :</label>
                                                                    <div class="col-sm-8 col-md-8">
                                                                        <input type="text" class="form-control" id="tel" placeholder="Téléphone">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="ville" class="col-sm-4 col-md-4 col-form-label">Ville :</label>
                                                                    <div class="col-sm-8 col-md-8">
                                                                        <select class="listecommune form-control mw-100 m-0" id="ville"  name="ville" required>
                                                                            <option></option>
                                                                            <!-- On parcourt la liste des lieux -->
                                                                           
                                                                            <?php foreach ($lesLieux as $lieu): ?>
                                                                                <?php if ($lieu['id_lieu']==$ville['id_lieu']): ?>
                                                                                    <option value="<?php echo $lieu['id_lieu']; ?>" selected><?php echo $lieu['nom_lieu']." (".$lieu['cp'].")"; ?></option>
                                                                                <?php else: ?>
                                                                                    <option value="<?php echo $lieu['id_lieu']; ?>"><?php echo $lieu['nom_lieu']." (".$lieu['cp'].")"; ?></option>
                                                                                <?php endif ?>
                                                                            <?php endforeach ?>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="description" class="col-sm-4 col-md-4 col-form-label">Description :</label>
                                                                    <div class="col-sm-8 col-md-8">
                                                                        <textarea class="form-control" id="description"></textarea>
                                                                    </div>
                                                                </div>

                                                                <div class="my-4 align-center">
                                                                    <button type="submit" data-target="#demo" data-slide-to="2" id="etape2" class="btn btn-primary p-2">Enregistrer et continuer</button>
                                                                </div>

                                                                <div class="text-center my-2">
                                                                    <small>Complétez votre profil plus tard. <a href="../">Retour à la page d'accueil</a></small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="carousel-item">
                                                <div class="col-md-8 offset-md-2 text-left mt-5">
                                                    <form action="#">
                                                        <br>
                                                        <div class="card border">
                                                            <h5 class="card-header">3/4 - Quelles sont vos compétences qui pourraient intéresser les membres de nom du site ?</h5>
                                                            <div class="card-body">
                                                                        
                                                                <div class="form-group row">
                                                                    <?php foreach ($lesCompetences as $competence): ?>
                                                                        <div class="col-sm-6 col-md-6" id="sexe">
                                                                            <div class="form-check bg-light m-1">
                                                                                <input class="form-check-input " name="competences" type="checkbox" value="<?php echo $competence['id_competence']; ?>" id="<?php echo 'comp_'.$competence['id_competence']; ?>">
                                                                                <label class="form-check-label" for="<?php echo 'comp_'.$competence['id_competence']; ?>">
                                                                                    <?php echo $competence['lib_competence']; ?>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    <?php endforeach ?>
                                                                </div>

                                                                <div class="my-4 align-center">
                                                                    <input type="hidden" id="userID" value="<?php echo $user['id_utilisateur'];?>">
                                                                    <button type="submit" data-target="#demo" data-slide-to="3" id="etape3" class="btn btn-primary p-2">Enregistrer et continuer</button>
                                                                </div>

                                                                <div class="text-center my-2">
                                                                    <small>Complétez votre profil plus tard. <a href="../">Retour à la page d'accueil</a></small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>


                                            <div class="carousel-item">
                                                <div class="col-md-8 offset-md-2 text-left mt-5">
                                                    <form action="../profil/?profil" method="POST">
                                                        <br>
                                                        <div class="card border">
                                                            <h5 class="card-header">4/4 - Pour finir, téléchargez votre photo pour illustrer votre profil.</h5>
                                                            <div class="card-body">
                                                                        
                                                                <div class="form-group row">
                                                                    <div class="col-sm-6 col-md-6">
                                                                        <img src="https://saccade.ca/img/autiste-apropos.svg" class="w-50 p-0 border rounded-circle" name="img_profil" id="img_profil">
                                                                    </div>
                                                                    <div class="col-sm-6 col-md-6">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="form-control" id="photo">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="my-4 align-center">
                                                                    <button type="submit" class="btn btn-primary p-2" id="etape4" name="insert_img">Enregistrer</button>
                                                                </div>

                                                                <div class="text-center my-2">
                                                                    <small>Complétez votre profil plus tard. <a href="../">Retour à la page d'accueil</a></small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div> 
                                <?php elseif(isset($_GET['user_id']) && $_GET['user_id'] == md5($user['id_utilisateur']) && !empty($_GET[md5('compteValide')])): ?>
                                    <?php $compte = "true"; ?>
                                    <?php if ($user['validation_compte']=="1"): ?>
                                        <?php $compte = "false"; ?>
                                    <?php endif ?>

                                    <?php if ($_GET[md5('compteValide')] == $compte): ?>
                                        <?php $Utilisateur->UpdateValidationUtilisateur($user['id_utilisateur'],"1"); ?>
                                        <div class="card text-center">
                                            <div class="card-body">
                                                <h5 class="card-title">
                                                    Votre compte est maintenant activté !
                                                </h5>
                                                <a href="index.php?<?php echo "user_id=".md5($user['id_utilisateur'])."&".md5('compteValide')."="; ?>" class="btn btn-primary">Finir l'inscription ?</a>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="card text-center">
                                            <div class="card-body">
                                                <h5 class="card-title">
                                                    Votre compte est déjà activé !
                                                </h5>
                                                <a href="../" class="btn btn-primary">Revenir à la page d'accueil</a>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                <?php else: ?>
                                    <div class="card text-center">
                                        <div class="card-body">
                                            <h5 class="card-title">
                                                Votre compte est déjà activé !
                                            </h5>
                                            <a href="../" class="btn btn-primary">Revenir à la page d'accueil</a>
                                        </div>
                                    </div>
                                <?php endif ?>
                                <?php 
                                    //var_dump($_GET['user_id'],$user['id_utilisateur'],$_GET[md5('compteValide')],md5($user['id_utilisateur'])); 
                                ?>
                            </div>
                        </div>
                                
                    </div>
                </div>
            </div>
        </section>

        <!-- Section footer -->
        <?php 
            require_once "../footer/footer_sous-racine.php";
        ?>
        <!-- Fin section footer -->
        <script src="../assets/web/assets/jquery/jquery.min.js"></script>
        <script src="../assets/popper/popper.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/smoothscroll/smooth-scroll.js"></script>
        <script src="../assets/dropdown/js/nav-dropdown.js"></script>
        <script src="../assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="../assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="../assets/parallax/jarallax.min.js"></script>
        <script src="../assets/as-pie-progress/jquery-as-pie-progress.min.js"></script>
        <script src="../assets/mbr-flip-card/mbr-flip-card.js"></script>
        <script src="../assets/tether/tether.min.js"></script>
        <script type="text/javascript" src="../assets/monjs/disponibilite.js"></script>
        <script type="text/javascript">
            $('.carousel').carousel({
                interval: false
            }); 
        </script>
        <!-- Select 2 -->
        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                $('.listecommune').select2({
                    placeholder: "Code postal / Lieu",
                    allowClear: true
                });
            });
        </script>
        <!-- Finalisation de compte -->
        <script type="text/javascript" src="../assets/monjs/finalisation_compte.js"></script>
    </body>
</html>
