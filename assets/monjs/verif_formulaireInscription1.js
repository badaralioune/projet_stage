$(function(){
    /*Les variables*/
    var statut = $('#statut'),
    siret = $('#siret'),
    raison_sociale = $('#raison_sociale');
    var pseudo = $('#pseudo'), email = $('#email'), blocInscrit2 = $('#blocInscription2'), 
    blocBouton = $('#btnblocInscription1'), lebtnContinuer = $('#continuerInscription');
    /*Bloc message erreur*/
    var msg_erreur_siret = $('#erreur_siret'), msg_erreur_pseudo = $('#erreur_pseudo'), msg_erreur_email= $('#erreur_email');


    /*Mettons invisible le bloc 2*/
    blocInscrit2.hide();

        
    /*En cliquant sur le bounton continuer l'inscription*/
    lebtnContinuer.click(function(){

        /*Vérification des champs qui ont des liens avec la base de données*/
        $.get( 
            "verif_donneesInscription.php",{
                lepseudo : pseudo.val(),
                lemail : email.val(),
                lesiret : siret.val()
            }, 
            function(donnees) {
                /*On récupère les données en JSON*/
                var obj = JSON.parse(donnees);

                verifDonnees(obj.unpseudo, obj.unemail, obj.unsiret);
            }
        );
    })


    /*Fonction retour de la méthode get */
    function verifDonnees(le_pseudo, le_email, le_siret){
        var continuer = true;

        /*Vérifie les champs*/
        var champs = [$('#statut'), siret, raison_sociale, pseudo, email];
        /*On parcourt les champs et on enleve les bordures en rouge et les messages d'erreur*/
        for (var i = 0 ; i < champs.length; i++) {
            champs[i].css("border-style","none");
            $('.verif'+i).empty();
        }

        /*Si le pseudo est existant*/
        if (le_pseudo != "") {
            msg_erreur_pseudo.empty().append("<ul class='mb-0'><li>Votre pseudo est déjà associé à un autre compte !</li></ul>")
            pseudo.css("border","1px solid red");
            continuer = false;
        }

        /*Si l'email est existant*/
        if (le_email != "") {
            msg_erreur_email.empty().append("<ul class='mb-0'><li>Votre adresse e-mail est déjà associée à un autre compte !</li></ul>")
            email.css("border","1px solid red");
            continuer = false;
        }

        /*Si le n°SIRET est existant*/
        if (le_siret != "") {
            msg_erreur_siret.empty().append("<ul class='mb-0'><li>Votre numéro de SIRET est déjà associée à un autre compte !</li></ul>")
            siret.css("border","1px solid red");
            continuer = false;
        }

        
        for (var i = 0 ; i < champs.length; i++) {
            if (champs[i].attr("required") && champs[i].val() == "") {
                champs[i].css("border","1px solid red");
                $('.verif'+i).empty().append("<ul class='mb-0'><li>Veuillez renseigner ce champ.</li></ul>")
                continuer = false;
            }
        }


        if (continuer == true) {
            blocBouton.hide();
            blocInscrit2.show();
        }
        
    }
});
