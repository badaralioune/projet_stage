$(function(){
    /*CheckBox Période et le boc période*/
    var periode = $('#periode'), bloc_periode = $('#bloc_periode');

    /*CheckBox Jours de la semaine et le bloc période*/
    var jour_semaine = $('#jour_semaine'), bloc_jour_semaine = $('#bloc_jour_semaine');

    /*Jours de la semaine*/
    var lundi = $('#lundi'), mardi = $('#mardi'), mercredi = $('#mercredi'), jeudi = $('#jeudi'), vendredi = $('#vendredi'), samedi = $('#samedi'), dimanche = $('#dimanche');
    
    /*Fonction pour cacher le bloc période*/
    function cacherBlocPeriode(){
        /*On cache le le bloc*/
        bloc_periode.hide();
        /*Si action*/
        periode.change(function(){
            /*Si on coche*/
            if($(this).prop("checked")==true){ 
                /*On affiche le bloc*/
                bloc_periode.show();
                /*On met les required pour la vérification des saisies*/
                $('#debut').prop('required',true);
                $('#fin').prop('required',true);
            }else{ //Sinon 
                /*On cache le le bloc*/
                bloc_periode.hide();
                /*On enleve les required pour la vérification des saisies*/
                $('#debut').prop('required',false);
                $('#fin').prop('required',false);
            }
        })
    }
    /*Execution*/
    cacherBlocPeriode();


    /*Fonction pour cacher le bloc période*/
    function cacherBlocJoursSemaine(){
        /*On cache le le bloc*/
        bloc_jour_semaine.hide();
        /*Si action*/
        jour_semaine.change(function(){
            /*Si on coche*/
            if($(this).prop("checked")==true){ 
                /*On affiche le bloc*/
                bloc_jour_semaine.show();
                /*On crée le tableau des checkboxs*/
                var lesjours = [lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche];
                /*On met les required pour la vérification des jours cochés*/
                for (var i = 0; i < lesjours.length; i++) {
                    
                    /*On cache les champs pour les heures si le jour n'est pas coché*/
                    if (!lesjours[i].prop("checked")) {
                        $('.bloc_'+lesjours[i].attr("id")).hide();
                    }else{
                        $('.bloc_'+lesjours[i].attr("id")).show();
                    }
                    /*Si on choisi une case*/
                    lesjours[i].change(function(){
                        var hdebut = $('#h_debut_'+$(this).attr("id")), hfin = $('#h_fin_'+$(this).attr("id"));
                        /*hdebut.attr("max",hfin.val())*/
                        console.log(hdebut.val()+$(this).attr("id"))
                        /*Si elle est coché*/
                        if($(this).prop("checked")==true){
                            /*On affiche les champs et on mets les vérifs*/
                            $('.bloc_'+$(this).attr("id")).show();
                            hdebut.prop('required',true);
                            hfin.prop('required',true);
                            
                            /*Si on change l'heure début*/
                            hdebut.on('change',function(){
                                if($(this).attr('max') != hfin.val()){
                                    $(this).attr("max",hfin.val())
                                }
                            })
                            /*Si on change l'heure fin */
                            hfin.on('change',function(){
                                if($(this).attr('min') != hdebut.val()){
                                    $(this).attr("min",hdebut.val())
                                }
                            })
                        }else{/*Sinon*/
                            $('.bloc_'+$(this).attr("id")).hide();
                            hdebut.prop('required',false);
                            hfin.prop('required',false);
                        }
                    })
                }

            }else{ //Sinon 
                /*On cache le le bloc*/
                bloc_jour_semaine.hide();
                /*On enleve les required pour la vérification des saisies*/
            }
        })
    }
    /*Execution*/
    cacherBlocJoursSemaine();

})