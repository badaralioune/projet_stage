$(function(){

    /*Lorsqu'on envoie le formulaire*/
    $("#formulaireIns").submit(function(event){
        /*Les champs*/
        var mdp1 = $('#pwd1'), mdp2 = $('#pwd2'), pseudo = $('#pseudo'), email = $('#email');
        
        /*Les variables des messages erreurs*/
        var msg_erreur_mdp1 = $('#erreur_mdp1'), msg_erreur_mdp2 = $('#erreur_mdp2'), msg_erreur_mdpdifferents = $('#erreur_mdpdifferents');

        /*Variable qui vérifie pour envoyer les données*/
        var envoyer = true;

        // Le nombre de caractère de chaque champs
        var caractere_mdp1 = mdp1.val().length;
        var caractere_mdp2 = mdp2.val().length;

        /*Si mot de passe ou confirmation de mot passe est < 6 caractères*/
        if (caractere_mdp1<6 || caractere_mdp2<6) {
            /*Si mot de passe 1*/
            if (caractere_mdp1<6) {
                msg_erreur_mdp1.empty().append("<ul class='mb-0'><li>Votre mot de passe doit conternir au moins 6 caractères !</li></ul>");
                mdp1.css("border","1px solid red");
            }else{
                msg_erreur_mdp1.empty();
            }

            /*Si mot de passe 2*/
            if (caractere_mdp2<6) {
                msg_erreur_mdp2.empty().append("<ul class='mb-0'><li>Votre confirmation de mot de passe doit conternir au moins 6 caractères !</li></ul>");
                mdp2.css("border","1px solid red");
            }else{
                msg_erreur_mdp2.empty();
            }
            console.log(pseudo.val());
            envoyer = false;

        }else{
            msg_erreur_mdp1.empty();
            msg_erreur_mdp2.empty();
            mdp1.css("border-style","none");
            mdp2.css("border-style","none");
        }

        /*Si les deux mots de passe sont différents*/
        if (mdp1.val()!= mdp2.val()) {

            msg_erreur_mdpdifferents.empty().append("<ul class='mb-0'><li>Le mot de passe est différent de sa confirmation !</li></ul>");
            /*Ajouter des bordures alert*/
            mdp1.css("border","1px solid red");
            mdp2.css("border","1px solid red");
            envoyer = false;
        }else{
            msg_erreur_mdpdifferents.empty()
            mdp1.css("border-style","none");
            mdp2.css("border-style","none");
        }

        if (pseudo.val() != "" && email.val() != "") {
            console.log(pseudo.val());
            /*Vérification des champs qui ont des liens avec la base de données */
            $.get( 
                "verif_donneesInscription.php",{
                    lepseudo : pseudo.val(),
                    lemail : email.val(),
                    lesiret : siret.val()
                }, 
                function(donnees) {
                    /*On récupère les données en JSON*/
                    var obj = JSON.parse(donnees);
                    console.log(envoyer)
                    //verifDonnees(obj.unpseudo, obj.unemail, obj.unsiret);
                }
            );
        }
        event.preventDefault();
        if (envoyer === false) {
            event.preventDefault();
        }
    })
})