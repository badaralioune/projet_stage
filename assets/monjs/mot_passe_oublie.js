$(function(){
	var btn_mdpoublie = $('#mdp_oublie');

	btn_mdpoublie.click(function(){
		swal({
		  content: "input",
		  title: "Récupération de votre mot de passe",
		  text: "Pour demander un nouveau mot de passe, merci de renseigner votre adresse e-mail ci-dessous. Un e-mail avec un nouveau mot de passe vous sera envoyé.",
		  button: "Réinitialiser mon mot de passe",
		})
		.then((value) => {
			if (!value) {
				
			}else{
				

		  		$.get(
			  		"mdp_oublie.php",
			  		{
			  			lemail : value
			  		},
			  		function(data){
			  			var reponse = JSON.parse(data);
			  			if (reponse.resultat == "ok") {
			  				swal("Le compte associé à cette adresse est introuvable.", 
								"error", {
						  		button: "Fermer",
							});
			  				
			  			}else{
			  				swal("Nous vous avons envoyé par courriel les instructions pour changer de mot de passe, pour autant qu'un compte existe avec l'adresse "+reponse.resultat+". Vous devriez recevoir rapidement ce message.", 
								"Si vous ne recevez pas de message, vérifiez que vous avez saisi l'adresse avec laquelle vous vous êtes enregistré et contrôlez votre dossier de spam.", 
								"success", {
						  		button: "Fermer",
							});
			  			}
			  		}
			  	);
		  	}

	
		});
	})

})	