<?php 
    require_once "config/fonctions.php";
    $connexion = false;
    $user = [];
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        if (isset($_SESSION['id_utilisateur'])) {
            $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
            $connexion = true;
        }
    }
?>
<!DOCTYPE html>
<html  lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="assets/images/logo2.png" type="image/x-icon">
        <meta name="description" content="">
        <title>Toutes les annonces</title>
        <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="assets/socicon/css/styles.css">
        <link rel="stylesheet" href="assets/dropdown/css/style.css">
        <link rel="stylesheet" href="assets/as-pie-progress/css/progress.min.css">
        <link rel="stylesheet" href="assets/tether/tether.min.css">
        <link rel="stylesheet" href="assets/theme/css/style.css">
        <link rel="preload" as="style" href="assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
        <!-- select 2 -->
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="assets/moncss/moteur_recherche.css">
        <style>
            .uneannonce:hover{
                
            }
        </style>
    </head>
    <body>
        <section class="menu cid-rVmtRueE1z" id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href=""><img src="assets/images/logo2.png" alt="Nom du site" style="height: 3.8rem;"></a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-4" href="">NOM DU SITE</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-black display-4" href="#features1-8"><span class="mbri-bulleted-list mbr-iconfont mbr-iconfont-btn"></span>Toutes les demandes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="deposer_annonce/"><span class="mbri-edit mbr-iconfont mbr-iconfont-btn"></span>Déposer une annonce</a>
                        </li>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="profil/">
                                    <span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span> <?php echo $user['pseudo']; ?>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="authentification/sign_up.php"><span class="mbri-plus mbr-iconfont mbr-iconfont-btn"></span>Inscription</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="authentification/login.php"><span class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>Connexion</a>
                            </li>   
                        <?php endif ?>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="config/deconnexion.php"><span class="mbri-logout mbr-iconfont mbr-iconfont-btn"></span>Déconnexion</a>
                            </li>
                        <?php endif ?>
                    </ul>

                </div>
            </nav>
        </section>
        <section class="header15 cid-rVmx6gBoE2 mbr-parallax-background pb-3" id="header15-2">
            <div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(255, 255, 255);"></div>
            <div class="container align-center">
                <div class="row">
                    <div class="mbr-white col-lg-12 col-md-12 content-container">
                        <h1 class="mbr-section-title mbr-bold pb-2 mbr-fonts-style display-1">Proposez & trouvez</h1>
                        <h2 class="mbr-text pb-3 mbr-fonts-style display-5">
                            Le sous-titre
                        </h2>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="form-container rounded">
                            <div class="media-container-column" data-form-type="formoid">
                                <!---Formbuilder Form--->
                                <form action="index.php#features1-8" method="GET" class="mbr-form form-with-styler">
                                    <div class="row">
                                        <div class="col-md-6 form-group" id="bloc_categorie">
                                            <select class=" form-control w-100 m-0" id="categorie" name="categorie" required>
                                                <option value="all_Cat" selected>Toutes les catégories</option>
                                                <!-- On parcourt la liste des catégories -->
                                                <?php foreach ($lesCategories as $cat): ?>
                                                    <option value="<?php echo $cat['id_cat']; ?>"><?php echo $cat['lib_cat']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6 form-group" id="bloc_commune">
                                            <select class=" form-control w-100 m-0" name="commune" required>
                                                <option value="all_Com" selected>Toutes les communes</option>
                                                <!-- On parcourt la liste des communes -->
                                                <?php foreach ($lesLieux as $commune): ?>
                                                    <option value="<?php echo $commune['id_lieu']; ?>"><?php echo $commune['nom_lieu']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <!-- <div class="col-md-4 form-group ">
                                            <input type="text" name="mot_cle" placeholder="Mots clés" class="form-control px-3 display-7"  required>
                                        </div> -->
                                        <div class="col-md-12 input-group-btn">
                                            <button type="submit" class="btn btn-form btn-info display-4"><span class="mbri-search mbr-iconfont mbr-iconfont-btn"></span>Rechercher</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="features1 cid-rVmE73Fmjs cid-rVmyoJqfWE pt-3" id="features1-8">
            <div class="container">
                <h3 class="mbr-section-subtitle mbr-fonts-style display-5">Annonces services, jobbing et emploi de proximité - (<?php echo $nombreannonce; ?>)</h3>
                <!-- <p>Trouvez des compétences et des talents près de chez vous. Particuliers, professionnels, proposez vos services et vos talents.<br>Déposez une annonce gratuite.</p> -->
                <div class="row pt-3 mt-2">
                    <div class="card col-12 col-md-12 col-lg-12  h-100">
                        <div class="mt-0 h-100">
                            <!-- On parcourt la liste des annonces -->
                            <?php foreach ($lesAnnoncesByPage as $lannonce): ?>
                                <?php 
                                    /*le sigle de la table type de tarification*/
                                    $sigleTarifAnnonce = $TypeTarification->getTypeTarificationById($lannonce['id_type_tarif'])['sigle_type_tarif'];
                                    /*les infos de l'utilisateur qui publie de la table utilisateur*/
                                    $userAnnonce = $Utilisateur->getUtilisateurById($lannonce['id_utilisateur']);
                                    /*le lieu de la table Lieu*/
                                    $lieuAnnonce = $Lieu->getLieuById($lannonce['id_lieu']);
                                    /*Sous catégorie pour une annonce*/
                                    $sousCatAnnonce = $SousCategorie->getSousCategorieByAnnonce($lannonce['id_annonce'])['lib_sous_cat'];
                                    /*Catégorie pour une annonce*/
                                    $catAnnonce = $Categorie->getCategorieByAnnonce($lannonce['id_annonce'])['lib_cat'];
                                    /*Le format de la date de publication annonce*/
                                    $dateFormatAnnonce = dateFormatAnnoncePub($lannonce['id_annonce']);
                                    /*Crypter l'id_annonce
                                    $crypter = new Crypter($lannonce['id_annonce']);
                                    $chaine_crypter = $crypter->encrypt($lannonce['id_annonce']);*/


                                ?>
                                <div class="card-body border rounded mt-2 p-1 pt-2 uneannonce" >
                                    <a href="annonce/<?php echo enleverCaracteresSpeciaux($sousCatAnnonce).'/'.enleverCaracteresSpeciaux($lannonce['lib_annonce']).'-'.$lannonce['id_annonce'].'/'; ?>" >
                                        <div class="row px-2">
                                            <div class="col-md-6 col-6 col-sm-6">
                                                <small class="text-muted"><?php echo $dateFormatAnnonce; ?></small>
                                            </div>
                                            <div class="col-md-6 col-6 col-sm-6">
                                                <strong class="p-2 text-secondary float-right">
                                                    <?php echo $lannonce['prix']."€"; ?>
                                                    <!-- si Une annonce est gratuit ou sur devis, on affiche que le type de tarification -->
                                                    <!-- <?php if ($lannonce['prix'] == 0 && $lannonce['id_type_tarif'] == 4 || $lannonce['id_type_tarif'] == 5): ?>
                                                        <?php if ($lannonce['id_type_tarif'] == 4 ): ?>
                                                            <?php echo $sigleTarifAnnonce; ?>
                                                        <?php endif ?>
                                                        <?php if ($lannonce['id_type_tarif'] == 5 ): ?>
                                                            <?php echo $sigleTarifAnnonce; ?>
                                                        <?php endif ?>
                                                    <?php else: ?>
                                                        <?php echo $lannonce['prix']."€ / ".$sigleTarifAnnonce; ?>
                                                    <?php endif ?> -->
                                                </strong>
                                            </div>
                                        </div>
                                        <div class="row px-2">
                                            <div class="col-md-3 col-3 col-sm-3">
                                                <img src="assets/images/product1.jpg" class="img-fluid" alt="Nom du site">
                                            </div>
                                            <div class="col-md-9 col-9 col-sm-9">
                                                <strong><?php echo $userAnnonce['pseudo']; ?></strong>
                                                <br>
                                                <h4 class="text-left"><?php echo $lannonce['lib_annonce']; ?></h4>
                                                <p class="text-muted" style="text-align: left;">
                                                    <?php 
                                                        /*Nombre de mots*/
                                                        $nb_mots = 20;
                                                        /*la description*/
                                                        $var = $lannonce['description']; 
                                                        /* Scinde une chaîne de caractères en segments*/
                                                        $tab= explode(' ', $var, $nb_mots+1);
                                                        /*Détruit une variable*/
                                                        unset($tab[$nb_mots]);
                                                        /*Rassemble les éléments d'un tableau en une chaîne on affiche*/
                                                        echo implode(' ', $tab).'...';
                                                    ?>

                                                </p>
                                            </div>
                                        </div>
                                        <hr class="my-1">
                                        <div class="d-flex justify-content-around my-0">
                                          <small class="p-0"><span class="mbri-pin"></span> <?php echo $lieuAnnonce['nom_lieu']." (".$lieuAnnonce['cp'].")" ; ?></small>
                                          <small class="p-0"><span class="mbri-sale"></span> <?php echo $sousCatAnnonce; ?></small>
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach ?>
                        </div>
                        <div class="p-3 h-0 row justify-content-center">
                            <?php if ($lenbAnnonces>0): ?>
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">

                                        <!--  -->

                                        <?php if (isset($_GET['commune']) && isset($_GET['categorie'])){ ?>

                                            <?php if (isset($_GET['pag']) && $_GET['pag']>1): ?>
                                                <li class="page-item"><a class="page-link" href="index.php?pag=<?php echo $_GET['pag']-1;?>&commune=<?php echo $id_lieu;?>&categorie=<?php echo $id_cat;?>&souscategorie=<?php echo $id_sous_cat;?>#features1-8">&lt;</a></li>
                                            <?php else: ?>
                                                <li class="page-item"><a class="page-link" href="index.php?pag=1&commune=<?php echo $id_lieu;?>&categorie=<?php echo $id_cat;?>&souscategorie=<?php echo $id_sous_cat;?>#features1-8">&lt;</a></li>
                                            <?php endif ?>

                                            <?php  for ($i=1; $i <= $nbPage ; $i++) { ?>
                                                    <!-- Si on est dans tel page on met active -->
                                                    <?php if ($i == $Page1): ?>
                                                        <li class="page-item active"><a class="page-link " href="index.php?pag=<?php echo $i;?>&commune=<?php echo $id_lieu;?>&categorie=<?php echo $id_cat;?>&souscategorie=<?php echo $id_sous_cat;?>#features1-8"><?php echo $i; ?></a></li>
                                                    <?php else: ?>
                                                        <li class="page-item"><a class="page-link" href="index.php?pag=<?php echo $i;?>&commune=<?php echo $id_lieu;?>&categorie=<?php echo $id_cat;?>&souscategorie=<?php echo $id_sous_cat;?>#features1-8"><?php echo $i; ?></a></li>
                                                    <?php endif ?>
                                            <?php } ?>
                                            <?php if (isset($_GET['pag']) && $_GET['pag']< $nbPage): ?>
                                                <li class="page-item"><a class="page-link" href="index.php?pag=<?php echo $_GET['pag']+1; ?>&commune=<?php echo $id_lieu;?>&categorie=<?php echo $id_cat;?>&souscategorie=<?php echo $id_sous_cat;?>#features1-8">&gt;</a></li>
                                            <?php else: ?>
                                                <li class="page-item"><a class="page-link" href="index.php?pag=<?php echo $nbPage; ?>&commune=<?php echo $id_lieu;?>&categorie=<?php echo $id_cat;?>&souscategorie=<?php echo $id_sous_cat;?>#features1-8">&gt;</a></li>
                                            <?php endif ?>
                                        <!-- Sinon -->
                                        <?php }else{ ?> 
                                            <?php if (isset($_GET['pag']) && $_GET['pag']>1): ?>
                                                <li class="page-item"><a class="page-link" href="index.php?pag=<?php echo $_GET['pag']-1;?>#features1-8">&lt;</a></li>
                                            <?php else: ?>
                                                <li class="page-item"><a class="page-link" href="index.php?pag=1#features1-8">&lt;</a></li>
                                            <?php endif ?>
                                            
                                            <?php  for ($i=1; $i <= $nbPage ; $i++) { ?>
                                                <!-- Si on est dans tel page on met active -->
                                                <?php if ($i == $Page1): ?>
                                                    <li class="page-item active"><a class="page-link " href="index.php?pag=<?php echo $i;?>#features1-8"><?php echo $i; ?></a></li>
                                                <?php else: ?>
                                                    <li class="page-item"><a class="page-link" href="index.php?pag=<?php echo $i;?>#features1-8"><?php echo $i; ?></a></li>
                                                <?php endif ?>
                                                    
                                            <?php } ?>
                                            <?php if (isset($_GET['pag']) && $_GET['pag']< $nbPage): ?>
                                                <li class="page-item"><a class="page-link" href="index.php?pag=<?php echo $_GET['pag']+1; ?>#features1-8">&gt;</a></li>
                                            <?php else: ?>
                                                <li class="page-item"><a class="page-link" href="index.php?pag=<?php echo $nbPage; ?>#features1-8">&gt;</a></li>
                                            <?php endif ?>
                                        <?php } ?>

                                    </ul>
                                </nav>
                            <?php else: ?>
                                <h3 class="text-primary text-center">Vous n'avez pas trouvé <span class="text-secondary"> ce que vous cherchiez ?</span></h3>
                            <?php endif ?>
                                
                        </div>
                    </div>
                    <!-- <div class="card col-12 col-md-3 col-lg-3 order-md-1">
                        <div >
                            <div class="card-body border rounded ">
                                <p>
                                    Affiner vos critères :
                                </p>
                                <h5>Demandes de services</h5>
                                <div class="list-group list-group-flush">
                                    <a href="#" class="list-group-item list-group-item-action"><small>Bicolage - Travaux</small></a>
                                    <a href="#" class="list-group-item list-group-item-action"><small>Jardinage - Piscine</small></a>
                                    <a href="#" class="list-group-item list-group-item-action"><small>Déménagement - Manutention</small></a>
                                    <a href="#" class="list-group-item list-group-item-action"><small>Dépannage - Réparation de matériel</small></a>
                                </div>
                                <h5 class="mt-3">Demandes d'objets</h5>
                                <div class="list-group list-group-flush">
                                    <a href="#" class="list-group-item list-group-item-action"><small>Outillage & Travaux</small></a>
                                    <a href="#" class="list-group-item list-group-item-action"><small>Matériel de Jardin</small></a>
                                    <a href="#" class="list-group-item list-group-item-action"><small>Maison & Confort</small></a>
                                    <a href="#" class="list-group-item list-group-item-action"><small>Evénement, Réception & Fête</small></a>
                                </div>
                                <h5 class="mt-3">Demandes d'objets et de services près de chez vous</h5>
                                <div class="list-group list-group-flush">
                                    <a href="#" class="list-group-item list-group-item-action"><small>Paris</small></a>
                                    <a href="#" class="list-group-item list-group-item-action"><small>Marseille</small></a>
                                    <a href="#" class="list-group-item list-group-item-action"><small>Lyon</small></a>
                                    <a href="#" class="list-group-item list-group-item-action"><small>Toulouse</small></a>
                                </div>
                                
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>

        <!-- Section footer -->
        <?php 
            require_once "footer/footer_racine.php";
        ?>
        <!-- Fin section footer -->
        <script src="assets/web/assets/jquery/jquery.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/smoothscroll/smooth-scroll.js"></script>
        <script src="assets/dropdown/js/nav-dropdown.js"></script>
        <script src="assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="assets/parallax/jarallax.min.js"></script>
        <script src="assets/as-pie-progress/jquery-as-pie-progress.min.js"></script>
        <script src="assets/mbr-flip-card/mbr-flip-card.js"></script>
        <script src="assets/tether/tether.min.js"></script>
        <!-- Select 2 -->
        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                $('.listecategorie').select2({
                    placeholder: "-- Toutes les catégories --",
                    allowClear: true
                });
                $('.listecommune').select2({
                    placeholder: "Code postal / Lieu",
                    allowClear: true
                });
            });
        </script>
        <!-- Sous catégorie -->
        <!--<script type="text/javascript">
            $(function(){
                $("#categorie").change(function(){
                    var id_cat = $(this).val();
                    $.get(
                        "deposer_annonce/sous_cat.php",{
                            id_categoriee : id_cat
                        },function(donnees){
                            var cat = $("#bloc_categorie"), sous_cat = $("#bloc_sous_cat"), commune = $("#bloc_commune");
                            var tabCat = [cat, sous_cat, commune];
                            for (var i = 0; i<tabCat.length; i++) {
                                if(donnees){
                                    if (tabCat[i].hasClass("col-md-6")) {
                                        tabCat[i].removeClass("col-md-6").addClass("col-md-4").animate({maxWidth:"100%"})
                                    }
                                    sous_cat.addClass("col-md-4")
                                    sous_cat.empty().append(donnees);
                                }else{
                                    sous_cat.empty().append(donnees);
                                    sous_cat.removeClass("col-md-4");
                                    cat.addClass("col-md-6")
                                    commune.addClass("col-md-6")
                                } 
                            }
                        }
                    );
                })
            })
        </script>-->
        <?php if (isset($_GET['commune']) && isset($_GET['categorie'])): ?>
            <script type="text/javascript">
                $(function(){
                    if ($("#features1-8").hasClass("pt-3")) {
                        $("#features1-8").animate({
                            paddingTop: "+=74px"
                        },200);
                    }
                })
            </script>
        <?php endif ?>
    </body>
</html>
