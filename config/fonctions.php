<?php
	require_once "classes.php";
	
	/*Fonction permettant de calculer les notes en star(étoiles)*/
	function avisUser($id){
		$Noter = new Noter();

		$nb_note = $Noter->getNbNoter($id)['nbnote'];
		$moyenne_note = $Noter->getNbNoter($id)['mynnote'];?>

		<span class="">
		    <span class="text-success">
				<?php if ($moyenne_note == 1): ?> <!-- Si note égale 1 affiche 1 étoile -->
					<i class="fa fa-star"></i>
			        <i class="fa fa-star-o"></i>
			        <i class="fa fa-star-o"></i>
			        <i class="fa fa-star-o"></i>
			        <i class="fa fa-star-o"></i>
				<?php elseif($moyenne_note == 2): ?> <!-- Si note égale 2 affiche 2 étoiles -->
				    <i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star-o"></i>
			        <i class="fa fa-star-o"></i>
			        <i class="fa fa-star-o"></i>
				<?php elseif($moyenne_note == 3): ?> <!-- Si note égale 3 affiche 3 étoiles -->
					<i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star-o"></i>
			        <i class="fa fa-star-o"></i>	
				<?php elseif($moyenne_note == 4): ?> <!-- Si note égale 4 affiche 4 étoiles -->
					<i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star-o"></i>
				<?php elseif($moyenne_note == 5): ?> <!-- Si note égale 5 affiche 5 étoiles -->
					<i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
				<?php elseif (1< $moyenne_note && $moyenne_note < 2): ?> <!-- Si note égale 1 affiche 1 étoile -->
					<i class="fa fa-star"></i>
					<i class="fa fa-star-half-o"></i>
			        <i class="fa fa-star-o"></i>
			        <i class="fa fa-star-o"></i>
			        <i class="fa fa-star-o"></i>
				<?php elseif(2< $moyenne_note && $moyenne_note < 3): ?> <!-- Si note égale 2 affiche 2 étoiles -->
				    <i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star-half-o"></i>
			        <i class="fa fa-star-o"></i>
			        <i class="fa fa-star-o"></i>
				<?php elseif(3< $moyenne_note && $moyenne_note < 4): ?> <!-- Si note égale 3 affiche 3 étoiles -->
					<i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star-half-o"></i>
			        <i class="fa fa-star-o"></i>	
				<?php elseif(4< $moyenne_note && $moyenne_note < 5): ?> <!-- Si note égale 4 affiche 4 étoiles -->
					<i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star"></i>
			        <i class="fa fa-star-half-o"></i>
				<?php else: ?> <!-- Sinon 0  étoile -->
					<i class="fa fa-star-o"></i>
					<i class="fa fa-star-o"></i>
					<i class="fa fa-star-o"></i>
					<i class="fa fa-star-o"></i>
					<i class="fa fa-star-o"></i>
				<?php endif ?>

		    </span>
		    
		    (<?php echo $nb_note; ?> avis : <?php echo "<strong>". number_format($moyenne_note, 2, ',', ' ')."/5</strong>"; ?>)

		</span><?php 
	} 

	/*Fonction permettant de créer un compte utilisateur*/
	function createUser($mail, $tel, $pseudo, $mdp, $siret, $raison_sociale, $statut){
		
		$U = new Utilisateur();
		//On insére l'utilisateur dans la table Utilisateur
		$U->setUtilisateur($mail,$tel,$pseudo,$mdp);


		//Si ce n'est pas particulier
		if ($statut != "particulier") {
			// On insére statut professionnelle
			$Pro = new Professionnelle();
		 	$Pro->setProfessionnelle($mail,$tel,$pseudo,$mdp,$siret,$raison_sociale,$statut);
		}else{
			// On insére statut perticulier
			$P = new Particulier();
			$P->setParticulier($mail,$tel,$pseudo,$mdp);
		} 

		

		$user = $U->getUtilisateurByEmail($mail);
		//On envoie le mail
		mailConfirmationCompte($mail, $user['id_utilisateur'], $user['pseudo']); 

		?>
		

		<div class="col-md-8 offset-md-2 align-center">
            <div class="border pt-5" style="height: 450px;">
                <div class="pt-5">
                    <h2><img src="../assets/images/valider.png" class="img-fluid"></h2>
                    <h4 class="text-warning">Votre inscription a bien été prise en compte, merci. Vous recevrez parallèlement un e-mail de confirmation à <strong><?php echo $mail; ?></strong>.</h4> 

                    <h6 class="text-secondary">Attention, cet e-mail peut possiblement arriver dans vos spams.</h6>
                    <a href="../" class="btn btn-secondary">Ok</a>
                </div>
            </div>
        </div> <?php
	}

	/*Fonction pour la confirmation de la création d'un compte*/
	function mailConfirmationCompte($mailDestinataire, $monuser, $pseudo){
		// To
		$to = $mailDestinataire;
		 
		// Subject
		$subject = 'Création de compte - Validez votre adresse email';
		 
		 
		// Headers
		$headers = 'From: ne-pas-repondre@nomdusite.com <ne-pas-repondre@nomdusite.com>'."\r\n";
		$headers .= 'Mime-Version: 1.0'."\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8';
		$headers .= "\r\n";
		 
		 
		// Message HTML
		$msg = "";
		$msg .= '
		    <div style="padding:5px; width:600px; background-color:#E0EBF5; border:#000000 thin solid">
		    <div>
		    	<h2 style="color:#274E9C; ">Dernière étape pour la création de compte</h2>
		    </div>
		    <div style="margin-top:20px;">
		        <p> Bonjour '.$pseudo.',</p>

		        <p>Nous sommes ravis de vous accueillir sur Nomdusite.</p>

		        <p>Pour activer et finaliser votre création de compte, veuillez cliquer sur le lien suivant :</p>
		        <span style="align-text:center;">
		            <a href="http://moussa.ascmtsahara.fr/testmail/finalisation_compte/index.php?user_id='.md5($monuser).'&'.md5('compteValide').'=true" style="text-decoration: none; padding:4px; border:1px solid #000; background-color:#274E9C; color:#FFF;">Cliquer ici</a>
		        </span>
		        <p>Si vous ne voyez pas le message "Votre compte est maintenant activté !" en haut de page, utilisez l\'url ci-dessous :</p>
		        <a href="http://moussa.ascmtsahara.fr/testmail/finalisation_compte/index.php?user_id='.md5($monuser).'&'.md5('compteValide').'=true" style="padding:2px; text-decoration: underline dotted; background-color:#F08080;">http://moussa.ascmtsahara.fr/testmail/finalisation_compte/index.php?user_id='.md5($monuser).'&'.md5('compteValide').'='.md5("true").'</a>
		    </div>
		    <div  style="margin-top:40px;">
		    	<p>Cordialement,</p>
		        <p>L\'équipe nomdusite.com</p>
		    </div>
		    <div  style="margin-top:60px; width:100%; align-text:center; padding:3px; background-color:#D9D1D1; color:#9B0000;">
		        Ce message a été envoyé automatiquement. Nous vous remercions de ne pas répondre.
		    </div>
		</div>'."\r\n";
		 
		// Function mail()
		mail($to, $subject, $msg, $headers);		
	}

	/*Fonction pour la réinitilisaton d'un mot de passe*/
	function mailReintialisationMDP($mailDestinataire){
		$nouveauMDP = random_password(6);
		$Utilisateur = new Utilisateur();
		$Particulier = new Particulier();
		$Professionnelle = new Professionnelle();
		$Utilisateur->setUpdateMDPUtilisateur($mailDestinataire,md5($nouveauMDP));
		$Particulier->setUpdateMDPParticulier($mailDestinataire,md5($nouveauMDP));
		$Professionnelle->setUpdateMDPProfessionnelle($mailDestinataire,md5($nouveauMDP));
		// To
		$to = $mailDestinataire;
		 
		// Subject
		$subject = '[Nom du site] Demande de mot de passe';
		 
		 
		// Headers
		$headers = 'From: ne-pas-repondre@nomdusite.com <ne-pas-repondre@nomdusite.com>'."\r\n";
		$headers .= 'Mime-Version: 1.0'."\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8';
		$headers .= "\r\n";
		 
		 
		// Message HTML
		$msg = "";
		$msg .= '
		    <div style="padding:5px; width:600px; background-color:#E0EBF5; border:#000000 thin solid">

			    <div style="">
			        <p> Bonjour moussains,</p>

			        <p>Une demande de mot de passe a été effectuée pour votre compte sur nomdusite.com</p>

			        <p>Veuillez trouver ci-dessous les identifiants vous permettant de vous y connecter : </p>
			        <p>
			        	<ul>
			        		<li>Adresse e-mail : <strong>'.$mailDestinataire.'</strong></li>
			        		<li>Mot de passe : <strong>'.$nouveauMDP.'</strong></li>
			        	</ul>
			        </p>
			        <p>Pour toute remarque ou tout renseignement complémentaire, merci de nous écrire à contact@nomdusite.com.</p>
			    </div>
			    <div  style="margin-top:40px;">
			    	<p>À bientôt,</p>
			        <p>L\'équipe nomdusite.com</p>
			    </div>
			    <div  style="margin-top:60px; width:100%; align-text:center; padding:3px; background-color:#D9D1D1; color:#9B0000;">
			        Ce message a été envoyé automatiquement. Nous vous remercions de ne pas répondre.
			    </div>
			</div>'."\r\n";
		 
		// Function mail()
		mail($to, $subject, $msg, $headers);		
	}


	/*Random mot de passe*/
	function random_password($chars) {
	   $letters = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	   return substr(str_shuffle($letters), 0, $chars);
	}


	/*Insertion d'une annonce dans la base avec un mail qui convient*/
	function depotAnnonce($lib_annon, $description, $adresse, $dte_debut, $dte_fin, $prix, $id_type_tarif, $id_cat, $id_sous_cat, $id_user, $id_user_reserver, $id_lieu, $id_type_service, $tel){

		$Annonce = new Annonce();
		$Annonce->setAnnonce($lib_annon, $description, $adresse, $dte_debut, $dte_fin, $prix, $id_type_tarif, $id_user, $id_user_reserver, $id_lieu, $id_type_service, $tel);
		$derniereAnnonce = $Annonce->getDerniereAnnonce();
		
		$SousCat = new SousCategorie();
		//$cat = $SousCat->getSousCategorieByIdSousCat($id_sous_cat)['id_cat'];

		$ACSC = new AnnonCatSousCat();
		$ACSC->setAnnonCatSousCat($derniereAnnonce['id_annonce'], $id_cat, $id_sous_cat);
	}


	/*Fonction qui permet d'afficher le format de la date pour une annonce selon le jour*/
	function dateFormatAnnoncePub($id_annonce){
		$Annonce = new Annonce();

		/*Le tableau des données pour une annonce*/
		$formatdate = $Annonce->getFormatDateAnnoncePub($id_annonce);

		/*On récupère la différence entre la date de la pub d'annonce et celle d'aujourd'hui*/
		$datediff = $formatdate['dtediff'];
		$dateformat = "";

		/*Si moins d'un an*/
		if ($datediff<365) {

			if ($datediff<2) {
				if ($datediff==0) {
					$dateformat = "Aujoud'hui à ".$formatdate['date_jour'];
				}else{
					$dateformat = "Hier à ".$formatdate['date_jour'];
				}
			}
			elseif (2<=$datediff && $datediff<7) {
				$dateformat = $formatdate['date_semaine'];
			}
			elseif (7<=$datediff && $datediff<365) {
				$dateformat = $formatdate['date_mois'];
			}
		}else{
			$dateformat = $formatdate['date_annee'];
		}

		return $dateformat;
	}


	/*Format en français*/
	function dateFormatFrançais($dateInsert){
		try {
		    $date = new DateTime($dateInsert);
		} catch (Exception $e) {
		    echo $e->getMessage();
		    exit(1);
		}

		return $date->format('d/m/Y');
	}

	/*Format en Anglais*/
	function dateFormatAnglais($dateInsert){
		try {
		    $date = new DateTime($dateInsert);
		} catch (Exception $e) {
		    echo $e->getMessage();
		    exit(1);
		}

		return $date->format('Y-m-d');
	}


	/*Format en francais*/
	function dateFormatFrançaisFormatSix($dateInsert){
		try {
		    $date = new DateTime($dateInsert);
		} catch (Exception $e) {
		    echo $e->getMessage();
		    exit(1);
		}

		return $date->format('d-m-Y');
	}

	/*enleverCaracteresSpeciaux*/
	function remove_accent($str)
	{
	  	$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
	  	$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
	  return str_replace($a, $b, $str);
	}
	/*enleverCaracteresSpeciaux*/
	function enleverCaracteresSpeciaux($str)
	{
	  	return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'),
	  	array('', '-', ''), remove_accent($str)));
	}



	/*Fonction modification les compétences d'une personne*/
	function updateCompetences($id_user, $tableauCompetences){
		$chaine = "";
		foreach($tableauCompetences as $check)
		{
			if (empty($chaine)) {
				$chaine = "(".$id_user.",".$check.")";
			}else{
				$chaine .= ", (".$id_user.",".$check.")";
			}
		}
		$chaine = $chaine.";";

		$Competence_Utilisateur = new Competence_Utilisateur();
		//Suppression des compétences
		$Competence_Utilisateur->deleteComptence_Utilisateur($id_user);
		//Insertion des nouvelles compétences
		$Competence_Utilisateur->setComptence_Utilisateur($chaine);
	}

	/*Fonction modification les compétences d'une personne après l'inscritpion*/
	function updateCompetencesInscritpion($id_user, $chaine){
		
		$valeur = $chaine.";";

		$Competence_Utilisateur = new Competence_Utilisateur();
		//Suppression des compétences
		$Competence_Utilisateur->deleteComptence_Utilisateur($id_user);
		//Insertion des nouvelles compétences
		$Competence_Utilisateur->setComptence_Utilisateur($valeur);
	}


	




	
	/*20 annonces Par page */
	$parPage = 20;

	/* ----------------------- Nombre d'annonces comptés --------------------*/
	$id_lieu = "";
	$id_cat = "";
	$id_sous_cat = "";

	if (isset($_GET['commune']) && isset($_GET['categorie'])) {
		if ($_GET['commune'] != "all_Com") {
			$id_lieu = $_GET['commune'];
		}
		if ($_GET['categorie'] != "all_Cat") {
			$id_cat = $_GET['categorie'];
		}
		if (isset($_GET['souscategorie'])) {
			if ($_GET['souscategorie'] != "all_SousCat") {
				$id_sous_cat = $_GET['souscategorie'];
			}
		}
		
	}
	$lenbAnnonces = $Annonce->getNbAnnonceByPage($id_lieu, $id_cat, $id_sous_cat)['nbAnnonce'];
	/*---------------------Fin -----------------------------------------------*/

	/*Nombre de page*/
	$nbPage = ceil($lenbAnnonces/$parPage);
	/*Vérification de la page*/
	if (isset($_GET['pag']) && $_GET['pag']>0 && $_GET['pag']<=$nbPage) {
		$Page1 = $_GET['pag'];
	}else{
		$Page1 = 1;
	}

	/*Variable pour afficher le nombre d'annonce*/
	$nombreannonce = "";

	/* si Nombre d'annonce <= 1 */
	if ($lenbAnnonces==0 || $lenbAnnonces== 1) {
		$nombreannonce = $lenbAnnonces." annonce";
	}else{
		$nombreannonce = $lenbAnnonces." annonces";
	}

	/*Page suivante sur la pagination*/
	$NouvellePage = (($Page1-1)*$parPage);



	/*La liste des annonces par page*/
	$lesAnnoncesByPage = $Annonce->getAnnonceByPage($id_lieu,$id_cat,$id_sous_cat,$NouvellePage, $parPage);
