<?php 
    require_once "../config/classes.php";

    if (isset($_GET['id_categorie']) && $_GET['id_categorie']!="") { 
        $idCat = $_GET['id_categorie']; ?>
        <select class=" form-control w-100 m-0" id="souscategorie" name="souscategorie" required>
            <option value="" selected disabled>-- Choisissez une sous-catégorie --</option>
            <?php 
                $lesSousCats = $SousCategorie->getSousCategorieByIdCat($idCat);
            ?>
            <!-- On parcourt les sous-catégories par catégorie -->
            <?php foreach ($lesSousCats as $souscat): ?>
                <option value="<?php echo $souscat['id_sous_cat']; ?>"><?php echo $souscat['lib_sous_cat']; ?></option>
            <?php endforeach ?>
        </select>
<?php  }else{ echo ""; } 

    if (isset($_GET['id_categoriee']) && $_GET['id_categoriee']!="" && $_GET['id_categoriee']!="all_Cat") { 
        $idCat = $_GET['id_categoriee'];

        if ($idCat == "all_Cat") { 
        }else{?>
            <select class=" form-control w-100 m-0" id="souscategorie" name="souscategorie" required>
                <option value="all_SousCat" selected>Toutes les sous-catégories</option>
                <?php 
                    $lesSousCats = $SousCategorie->getSousCategorieByIdCat($idCat);
                ?>
                <!-- On parcourt les sous-catégories par catégorie -->
                <?php foreach ($lesSousCats as $souscat): ?>
                    <option value="<?php echo $souscat['id_sous_cat']; ?>"><?php echo $souscat['lib_sous_cat']; ?></option>
                <?php endforeach ?>
            </select>
        <?php
            
        } ?>
        
        
<?php  }else{ echo ""; } ?>