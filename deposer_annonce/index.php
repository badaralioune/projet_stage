<?php 
    require_once "../config/fonctions.php";
    $connexion = false;
    $user = [];
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        if (isset($_SESSION['id_utilisateur'])) {
            $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
            $connexion = true;
        }else{
            header("Location: ../authentification/login.php");
        }

    }

    $depot = true;
    
    if (empty($user['validation_compte'])) {
        $depot = false;
    }

?>

<!DOCTYPE html>
<html  lang="fr">
    <head>
        <?php if ($depot == false): ?>
            <meta http-equiv="refresh" content="5; url=../profil/?edit">
        <?php endif ?>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="../assets/images/logo2.png" type="image/x-icon">
        <meta name="description" content="">
        <title>Déposer une annonce</title>
        <link rel="stylesheet" href="../assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="../assets/socicon/css/styles.css">
        <link rel="stylesheet" href="../assets/dropdown/css/style.css">
        <link rel="stylesheet" href="../assets/as-pie-progress/css/progress.min.css">
        <link rel="stylesheet" href="../assets/tether/tether.min.css">
        <link rel="stylesheet" href="../assets/theme/css/style.css">
        <link rel="preload" as="style" href="../assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <style>
            .mon-overflow{
                height: 350px;
                overflow-y: scroll;
                overflow-x: hidden;
            }
            /*texte hr*/
            .mon-hr {
                /* centre verticalement les enfants entre eux */
                align-items: center;

                /* active flexbox */
                display: flex;

                /* garde le texte centré s’il passe sur plusieurs lignes ou si flexbox n’est pas supporté */
                text-align: center;
            }

            .mon-hr::before,
            .mon-hr::after {
                /* la couleur est volontairement absente ; ainsi elle sera celle du texte */
                border-top: .0625em solid;

                /* nécessaire pour afficher les pseudo-éléments */
                content: "";

                /* partage le reste de la largeur disponible */
                flex: 1;

                /* espace les traits du texte */
                margin: 0 .5em;
            }
            .cid-rVmE73Fmjs select.form-control, .cid-rVmE73Fmjs input.form-control{
              min-height: 10px;
              padding: 3px;
            }

            .cid-rVmE73Fmjs textarea.form-control{
              min-height: 152px;
            } 

            .login-page{
                opacity:1.0; 
            }        
        </style>
    </head>
    <body>
        <section class="menu cid-rVmtRueE1z" id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="../"><img src="../assets/images/logo2.png" alt="Nom du site" style="height: 3.8rem;"></a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-4" href="../">NOM DU SITE</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../#features1-8"><span class="mbri-bulleted-list mbr-iconfont mbr-iconfont-btn"></span>Toutes les demandes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-black display-4" href="./"><span class="mbri-edit mbr-iconfont mbr-iconfont-btn"></span>Déposer une annonce</a>
                        </li>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../profil/">
                                    <span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span> <?php echo $user['pseudo']; ?>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../authentification/sign_up.php"><span class="mbri-plus mbr-iconfont mbr-iconfont-btn"></span>Inscription</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../authentification/login.php"><span class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>Connexion</a>
                            </li>   
                        <?php endif ?>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../config/deconnexion.php"><span class="mbri-logout mbr-iconfont mbr-iconfont-btn"></span>Déconnexion</a>
                            </li>
                        <?php endif ?>
                    </ul>
                </div>
            </nav>
        </section>
        <section class="features1 cid-rVmE73Fmjs" id="depot_annonce">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 mt-5">
                        <div class="form-container">
                            <div class="media-container-column" data-form-type="formoid">
                                <?php if ($depot == false): ?>
                                    <div class="alert alert-danger text-center" role="alert">
                                        Veuillez activer votre compte pour déposer une annonce !
                                    </div>
                                <?php endif ?>
                                <h1 class="mon-hr">Déposer une annonce</h1>
                                <!---Formbuilder Form--->
                                <div class="col-md-12 text-left">
                                    <form action="previsualiser_annonce.php" method="POST" class="mbr-form form-with-styler mt-3" >
                                        <div class="row mb-3 align-center">
                                            <div class="form-check col-sm-5">
                                                <input class="form-check-input" type="radio" name="type_annonce" id="proposer_service" value="1" <?php if (isset($_POST['type_service']) && $_POST['type_service'] == 1): ?>
                                                    <?php echo "checked"; ?>
                                                <?php endif ?> required>
                                                <label class="form-check-label" for="proposer_service">Je propose des services</label>
                                            </div>
                                            <div><p class="text-center">ou</p></div>
                                            <div class="form-check col-sm-5">
                                                <input class="form-check-input" type="radio" name="type_annonce" id="chercher_service" value="2" <?php if (isset($_POST['type_service']) && $_POST['type_service'] == 2): ?>
                                                    <?php echo "checked"; ?>
                                                <?php endif ?> required>
                                                <label class="form-check-label" for="chercher_service">Je cherche un service</label>
                                            </div>
                                        </div>
                                        <hr>
                                        <!-- Titre de l'annonce -->
                                        <div class="form-group row">
                                            <label for="titre_annonce" class="col-sm-6 col-form-label">Titre de l'annonce * :</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="titre_annonce" value="<?php if(isset($_POST['lib_annon'])){echo $_POST['lib_annon'];} ?>" class="form-control form-control-sm" id="titre_annonce" placeholder="Titre de l'annonce" required>
                                            </div>
                                        </div>
                                        <!-- Catégorie -->
                                        <div class="form-group row">
                                            <label for="categorie" class="col-sm-6 col-form-label">Catégorie * :</label>
                                            <div class="col-sm-6">
                                                <select class="listecategorie form-control w-100 m-0" id="categorie" name="categorie" required>
                                                    <option value="" hidden>-- Catégorie -- </option>
                                                    <!-- On parcourt la liste des catégories -->
                                                    <?php foreach ($lesCategories as $cat): ?>
                                                        <option value="<?php echo $cat['id_cat']; ?>" <?php if (isset($_POST['id_cat']) && $cat['id_cat'] == $_POST['id_cat']){  echo "selected"; } ?>>
                                                            <?php echo $cat['lib_cat']; ?>
                                                         </option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- Sous Catégorie -->
                                        <div class="form-group row" id="bloc_sous_cat">
                                            <?php if (isset($_POST['modifier'])): ?>
                                                <label for="categorie" class="col-sm-6 col-form-label">Sous catégorie * :</label>
                                                <div class="col-sm-6">
                                                    <select class=" form-control w-100 m-0" id="souscategorie" name="souscategorie" required>
                                                        <option value="" selected disabled>-- Choisissez une sous-catégorie --</option>
                                                        <?php 
                                                            $lesSousCats = $SousCategorie->getSousCategorieByIdCat($_POST['id_cat']);
                                                        ?>
                                                        <!-- On parcourt les sous-catégories par catégorie -->
                                                        <?php foreach ($lesSousCats as $souscat): ?>
                                                            <option value="<?php echo $souscat['id_sous_cat']; ?>" <?php if (isset($_POST['id_sous_cat']) && $souscat['id_sous_cat'] == $_POST['id_sous_cat']){  echo "selected"; } ?>>
                                                                <?php echo $souscat['lib_sous_cat']; ?>
                                                            </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                        <!-- Description de l'annonce  -->
                                        <div class="form-group row">
                                            <label for="description" class="col-sm-6 col-form-label">Description * :</label>
                                            <div class="col-sm-6">
                                                <textarea  name="description" class="form-control form-control-sm" id="description" placeholder="Description de l'annonce" required><?php if(isset($_POST['description'])){echo $_POST['description'];} ?></textarea> 
                                            </div>
                                        </div>
                                        <!-- Type de tarification -->
                                        <!-- <div class="form-group row">
                                            <label for="type_tarif" class="col-sm-6 col-form-label">Type de tarification * :</label>
                                            <div class="col-sm-6">
                                                
                                                <select name="type_tarif" class="form-control form-control-sm" id="type_tarif" required>
                                                    <option value="" selected disabled>-- Type de tarification --</option>
                                                    
                                                    <?php foreach ($lesTypeTarifications as $type_tarif): ?>
                                                        <option value="<?php echo $type_tarif['id_type_tarif']; ?>" <?php if (isset($_POST['id_type_tarif']) && $type_tarif['id_type_tarif'] == $_POST['id_type_tarif']){  echo "selected"; } ?>><?php echo $type_tarif['lib_type_tarif']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div> -->
                                        <!-- Ville ou CP -->
                                        <div class="form-group row">
                                            <label for="ville" class="col-sm-6 col-form-label float-md-right">Ville ou Code postal * :</label>
                                            <div class="col-sm-6">
                                                <select class="listecommune form-control w-100 m-0 form-control-sm" name="ville" id="ville" required>
                                                    <option value="" hidden> -- Ville --</option>
                                                    <!-- On parcourt la liste des communes -->
                                                    <?php foreach ($lesLieux as $commune): ?>
                                                        <option value="<?php echo $commune['id_lieu']; ?>"  <?php if (isset($_POST['id_lieu']) && $commune['id_lieu'] == $_POST['id_lieu']){  echo "selected"; } ?> >
                                                            <?php echo $commune['nom_lieu'].' ('.$commune['cp'].')'; ?>
                                                        </option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- Adresse -->
                                        <!-- <div class="form-group row">
                                            <label for="adresse" class="col-sm-6 col-form-label">Adresse * :</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="adresse" class="form-control form-control-sm" id="adresse" placeholder="Adresse" required>
                                            </div>
                                        </div> -->
                                        <!-- Téléphone -->
                                        <div class="form-group row">
                                            <label for="tel" class="col-sm-6 col-form-label">Téléphone * :</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="tel" value="<?php if(isset($_POST['tel'])){echo $_POST['tel'];} ?>" class="form-control form-control-sm" id="tel" placeholder="Téléphone" required>
                                            </div>
                                        </div>
                                        <!-- Prix -->
                                        <div class="form-group row ">
                                            <label for="prix" class="col-sm-6 col-form-label">Prix * :</label>
                                            <div class="col-sm-6">
                                                <div class="input-group mb-3">
                                                    <input type="number" name="prix" step="0.01" min="00" class="form-control" <?php if(isset($_POST['prix'])){echo 'value="'.$_POST['prix'].'"';} ?> id="prix" placeholder="Prix" required>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">€</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Disponibilité -->
                                        <!-- <div class="form-group row ">
                                            <label class="col-sm-6 col-form-label">Disponibilités :</label>
                                            <div class="col-sm-6">
                                                <div class="form-check">
                                                    <input type="checkbox" name="periode" class="form-check-input" id="periode" <?php if(isset($_POST['periode']) && $_POST['periode'] == "on"){echo " checked";} ?>>
                                                    <label class="form-check-label" for="periode">Période </label>
                                                    <div class="m-3" id="bloc_periode">
                                                        <div class="row">
                                                            <div class="col-md-12 col-12">
                                                                <div class="row">
                                                                    <div class="col-md-6 col-12">
                                                                        <div class="form-group row">
                                                                            <label for="debut" class="col-3 col-form-label"><small>Du *</small></label>
                                                                            <div class="col-9">
                                                                                <input class="form-control" name="debut_periode" value="<?php if(isset($_POST['dte_debut'])){echo dateFormatFrançaisFormatSix($_POST['dte_debut']);} ?>" type="text" placeholder="jj-mm-aaaa" id="debut">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-12">
                                                                        <div class="form-group row">
                                                                            <label for="fin" class="col-3 col-form-label"><small>au *</small></label>
                                                                            <div class="col-9">
                                                                                <input class="form-control" name="fin_periode" value="<?php if(isset($_POST['dte_fin'])){echo dateFormatFrançaisFormatSix($_POST['dte_fin']);} ?>" type="text" placeholder="jj-mm-aaaa" id="fin">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>      
                                                            </div>
                                                        </div>                         
                                                    </div>
                                                </div>
                                                <div class="form-check">
                                                    <input type="checkbox" name="jour_semaine" class="form-check-input" id="jour_semaine" <?php if(isset($_POST['j_semaine']) && $_POST['j_semaine'] == "on"){echo " checked";} ?>>
                                                    <label class="form-check-label" for="jour_semaine">Jours de la semaine</label>
                                                    <div class="m-2 " id="bloc_jour_semaine">
                                                        <?php foreach ($lesJours as $jour): ?>
                                                            <div class="row">
                                                                <div class="form-check col-md-3">
                                                                    <input type="checkbox" class="form-check-input" name="id_jour_<?php echo $jour['lib_jour']; ?>" id="<?php echo $jour['lib_jour']; ?>" value="<?php echo $jour['id_jour']; ?>" 
                                                                        <?php 
                                                                            if(isset($_POST['lundi']) && $_POST['lundi'] == $jour['id_jour']){ echo "checked";} 
                                                                            if(isset($_POST['mardi']) && $_POST['mardi'] == $jour['id_jour']){ echo "checked";} 
                                                                            if(isset($_POST['mercredi']) && $_POST['mercredi'] == $jour['id_jour']){ echo "checked";} 
                                                                            if(isset($_POST['jeudi']) && $_POST['jeudi'] == $jour['id_jour']){ echo "checked";} 
                                                                            if(isset($_POST['vendredi']) && $_POST['vendredi'] == $jour['id_jour']){ echo "checked";} 
                                                                            if(isset($_POST['samedi']) && $_POST['samedi'] == $jour['id_jour']){ echo "checked";} 
                                                                            if(isset($_POST['dimanche']) && $_POST['dimanche'] == $jour['id_jour']){ echo "checked";} 
                                                                        ?>
                                                                    >
                                                                    <label class="form-check-label" for="<?php echo $jour['lib_jour']; ?>"><?php echo $jour['lib_jour']; ?></label>
                                                                </div>
                                                                <div class="col-md-9 bloc_<?php echo $jour['lib_jour']; ?>">
                                                                    <div class="form-group row">
                                                                        <label for="h_debut_<?php echo $jour['lib_jour']; ?>" class="col-2 col-form-label"><small>de</small></label>
                                                                        <div class="col-4">
                                                                            <input class="form-control" name="h_debut_<?php echo $jour['lib_jour']; ?>" type="time" value="<?php if(isset($_POST['lundi']) && $_POST['lundi'] == $jour['id_jour']){ echo $_POST['d_lundi'];}
                                                                                elseif(isset($_POST['mardi']) && $_POST['mardi'] == $jour['id_jour']){ echo $_POST['d_mardi'];}
                                                                                elseif(isset($_POST['mercredi']) && $_POST['mercredi'] == $jour['id_jour']){ echo $_POST['d_mercredi'];}
                                                                                elseif(isset($_POST['jeudi']) && $_POST['jeudi'] == $jour['id_jour']){ echo $_POST['d_jeudi'];}
                                                                                elseif(isset($_POST['vendredi']) && $_POST['vendredi'] == $jour['id_jour']){ echo $_POST['d_vendredi'];}
                                                                                elseif(isset($_POST['samedi']) && $_POST['samedi'] == $jour['id_jour']){ echo $_POST['d_samedi'];}
                                                                                elseif(isset($_POST['dimanche']) && $_POST['dimanche'] == $jour['id_jour']){ echo $_POST['d_dimanche'];}
                                                                                else{echo '08:30';}
                                                                            ?>" id="h_debut_<?php echo $jour['lib_jour']; ?>">
                                                                        </div>
                                                                        <label for="h_fin_<?php echo $jour['lib_jour']; ?>" class="col-2 col-form-label"><small>à</small></label>
                                                                        <div class="col-4">
                                                                            <input class="form-control" name="h_fin_<?php echo $jour['lib_jour']; ?>" type="time" value="<?php if(isset($_POST['lundi']) && $_POST['lundi'] == $jour['id_jour']){ echo $_POST['f_lundi'];}
                                                                                elseif(isset($_POST['mardi']) && $_POST['mardi'] == $jour['id_jour']){ echo $_POST['f_mardi'];}
                                                                                elseif(isset($_POST['mercredi']) && $_POST['mercredi'] == $jour['id_jour']){ echo $_POST['f_mercredi'];}
                                                                                elseif(isset($_POST['jeudi']) && $_POST['jeudi'] == $jour['id_jour']){ echo $_POST['f_jeudi'];}
                                                                                elseif(isset($_POST['vendredi']) && $_POST['vendredi'] == $jour['id_jour']){ echo $_POST['f_vendredi'];}
                                                                                elseif(isset($_POST['samedi']) && $_POST['samedi'] == $jour['id_jour']){ echo $_POST['f_samedi'];}
                                                                                elseif(isset($_POST['dimanche']) && $_POST['dimanche'] == $jour['id_jour']){ echo $_POST['f_dimanche'];}
                                                                                else{echo '17:30';}
                                                                            ?>" id="h_fin_<?php echo $jour['lib_jour']; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endforeach ?>
                                                         
                                                    </div>     
                                                </div>
                                            </div>
                                        </div> -->
                                        <!-- Button prévisualiser -->
                                        <div class="row align-center">
                                            <div class="col-md-6 offset-md-3 input-group-btn">
                                                <button <?php if ($depot == false): ?>disabled<?php endif ?> type="<?php if ($depot == true): ?>submit<?php endif ?>" name="previsualiser" class="btn btn-form btn-info display-4">Prévusialiser mon annonce</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                                
                    </div>
                </div>
            </div>
        </section>

        <!-- Section footer -->
        <?php 
            require_once "../footer/footer_sous-racine.php";
        ?>
        <!-- Fin section footer -->
        <script src="../assets/web/assets/jquery/jquery.min.js"></script>
        <!-- Datepicker -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript">
            $(function(){

                $( "#debut" ).datepicker({ 
                    dateFormat: 'dd-mm-yy',
                    defaultDate: "+1w", 
                    numberOfMonths: 2, 
                    changeMonth: true, 
                    changeYear: true, 
                    yearRange: '-1:+1', 
                    maxDate: '+1Y', 
                    minDate :  new Date(),
                    closeText: 'Fermer',
                    prevText: 'Précédent',
                    nextText: 'Suivant',
                    currentText: 'Aujourd\'hui',
                    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                    monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                    dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                    weekHeader: 'Sem.',
                    onClose: function( selectedDate ) { 
                        $( "#fin" ).datepicker( "option", "minDate", selectedDate ); 
                    }
                }); 


                $( "#fin" ).datepicker({ 
                    dateFormat: 'dd-mm-yy',
                    defaultDate: "+1w", 
                    numberOfMonths: 2, 
                    changeMonth: true, 
                    changeYear: true, 
                    maxDate: '+2Y', 
                    closeText: 'Fermer',
                    prevText: 'Précédent',
                    nextText: 'Suivant',
                    currentText: 'Aujourd\'hui',
                    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                    monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                    dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                    weekHeader: 'Sem.',
                    onClose: function( selectedDate ) { 
                        $( "#debut" ).datepicker( "option", "maxDate", selectedDate ); 
                    }

                }); 
            });            
        </script>
        <!-- Fin datepicker -->
        <script src="../assets/popper/popper.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/smoothscroll/smooth-scroll.js"></script>
        <script src="../assets/dropdown/js/nav-dropdown.js"></script>
        <script src="../assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="../assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="../assets/parallax/jarallax.min.js"></script>
        <script src="../assets/as-pie-progress/jquery-as-pie-progress.min.js"></script>
        <script src="../assets/mbr-flip-card/mbr-flip-card.js"></script>
        <script src="../assets/tether/tether.min.js"></script>
        <script type="text/javascript" src="../assets/monjs/disponibilite.js"></script>
        
        <!-- Select 2 -->
        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                /**/
                $('.listecategorie').select2({
                    placeholder: "-- Choisissez une catégorie --",
                    allowClear: true
                });
                /*Select 2 pour la ville*/
                $('.listecommune').select2({
                    placeholder: "Ville ou Code postal",
                    allowClear: true
                });
                
            });
        </script>
        <!-- Sous catégorie -->
        <script type="text/javascript">
            $(function(){
                $("#categorie").change(function(){
                    var id_cat = $(this).val();
                    $.get(
                        "sous_cat.php",{
                            id_categorie : id_cat
                        },function(donnees){
                            if(donnees){
                                $("#bloc_sous_cat").empty().append("<label for=\"categorie\" class=\"col-sm-6 col-form-label\">Sous catégorie * :</label><div class=\"col-sm-6\">"+donnees+"</div>");
                            }else{
                                $("#bloc_sous_cat").empty();
                            }
                            
                    });
                })
            })
        </script>
        <!-- Type de tarification -->
        <script type="text/javascript">
            $(function(){
                if ($("#type_tarif").val()== 4 || $("#type_tarif").val()== 5) {
                    $("#prix").attr("disabled","disabled").attr("value","0").removeAttr("required");
                }

                $("#type_tarif").change(function(){
                    if ($(this).val()== 4 || $(this).val()== 5 ) {
                        $("#prix").attr("disabled","disabled").attr("value","0").removeAttr("required");
                    }else{
                        $("#prix").removeAttr("disabled").attr("required","required");
                    }
                })
            }) 
        </script>
        <!-- Période et jour de la semaine si sont cochés  -->
        <script type="text/javascript">
            $(function(){
                if ($("#periode").prop("checked")) {
                    $("#bloc_periode").show();
                }

                if ($("#jour_semaine").prop("checked")) {
                    $("#bloc_jour_semaine").show();
                    $("#periode").prop('required',true);
                }
            })
        </script>
        <!-- Les jours de la semaine si sont cochés -->
        <?php if(isset($_POST['lundi']) && isset($_POST['mardi']) && isset($_POST['mercredi']) && isset($_POST['jeudi']) && isset($_POST['vendredi']) && isset($_POST['samedi']) && isset($_POST['dimanche'])){ ?>
            <script type="text/javascript">
                $(function(){
                    /*Jours de la semaine*/
                    var lundi = $('#lundi'), mardi = $('#mardi'), mercredi = $('#mercredi'), jeudi = $('#jeudi'), vendredi = $('#vendredi'), samedi = $('#samedi'), dimanche = $('#dimanche');
                    /*On crée le tableau des checkboxs*/
                    var lesjours = [lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche];
                    /*On met les required pour la vérification des jours cochés*/
                    for (var i = 0; i < lesjours.length; i++) {
                        /*On cache les champs pour les heures si le jour n'est pas coché*/
                        if (!lesjours[i].prop("checked")) {
                            $('.bloc_'+lesjours[i].attr("id")).hide();
                        }else{
                            $('.bloc_'+lesjours[i].attr("id")).show();
                            $('#h_debut_'+lesjours[i].attr("id")).prop('required',true);
                            $('#h_fin_'+lesjours[i].attr("id")).prop('required',true);
                        }

                        /*Si on choisi une case*/
                        lesjours[i].change(function(){
                            var hdebut = $('#h_debut_'+$(this).attr("id")), hfin = $('#h_fin_'+$(this).attr("id"));
                            /*Si elle est coché*/
                            if($(this).prop("checked")==true){
                                /*On affiche les champs et on mets les vérifs*/
                                $('.bloc_'+$(this).attr("id")).show();
                                hdebut.prop('required',true);
                                hfin.prop('required',true);
                                
                                /*Si on change l'heure début*/
                                hdebut.on('change',function(){
                                    if($(this).attr('max') != hfin.val()){
                                        $(this).attr("max",hfin.val())
                                    }
                                })
                                /*Si on change l'heure fin */
                                hfin.on('change',function(){
                                    if($(this).attr('min') != hdebut.val()){
                                        $(this).attr("min",hdebut.val())
                                    }
                                })
                            }else{/*Sinon*/
                                $('.bloc_'+$(this).attr("id")).hide();
                                hdebut.prop('required',false);
                                hfin.prop('required',false);
                            }
                        })
                    }

                })
            </script>
            
        <?php } ?>
    </body>
</html>
